package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.f2v.fitnesson.entidades.TipoTreino;

public interface TipoTreinoRepositorio extends JpaRepository<TipoTreino, Long>,
		JpaSpecificationExecutor<TipoTreino> {
	
	/*
	 * usado para pegar o tipo de treino utilizidado na FichaTreino e imprimir o exercicio di�rio.  
	 */
	@Query("SELECT t FROM TipoTreino t where t.nome = :nome")
	public TipoTreino buscaPeloNome(@Param("nome") String nome);
	
	/*
	 * utilizado para listar os tipos de treino na tela de pesquisa tipoTreino
	 */
	@Query("SELECT t FROM TipoTreino t where t.nome LIKE %:nome% ORDER BY t.nome")
	public List<TipoTreino> pesquisaPeloNome(@Param("nome") String nome);
	
	

}
