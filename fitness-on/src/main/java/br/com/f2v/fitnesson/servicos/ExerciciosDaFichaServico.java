package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.ExerciciosDaFicha;
import br.com.f2v.fitnesson.repositorios.ExercicioDaFichaRepositorio;


@Service
public class ExerciciosDaFichaServico {
	
	@Inject 
	private ExercicioDaFichaRepositorio exercicioFichaRepositorio;
	
	public ExerciciosDaFicha salvar(ExerciciosDaFicha exercicio){
		return this.exercicioFichaRepositorio.save(exercicio);
	}
	
	public List<ExerciciosDaFicha> listarTodosExercicioFicha(){
		return this.exercicioFichaRepositorio.findAll();
	}
	
	public List<ExerciciosDaFicha> exerciciosDaFicha(long idFicha,long idCliente){
		return this.exercicioFichaRepositorio.bucarExercicosDaFicha(idFicha,idCliente);
	}
	
	public List<ExerciciosDaFicha> exerciciosFichaAndamento(long idFicha, long idTipoTreino){
		return this.exercicioFichaRepositorio.bucarExercicosDaFichaAndamento(idFicha, idTipoTreino);
	}

}
