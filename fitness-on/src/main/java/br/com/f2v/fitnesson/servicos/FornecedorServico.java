package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Fornecedor;
import br.com.f2v.fitnesson.repositorios.FornecedorRepositorio;

@Service
public class FornecedorServico {

	@Inject
	private FornecedorRepositorio fornecedorRepositorio;
	
	public Fornecedor salvar(Fornecedor fornecedor){
		return fornecedorRepositorio.save(fornecedor);
	}
	
	public List<Fornecedor> buscaFornecedorNome(String nome){
		return fornecedorRepositorio.buscaPeloNome(nome);
	}
	
	public List<Fornecedor> listarTodos(){
		return this.fornecedorRepositorio.findAll();
	}
	
	public Fornecedor buscaPorId(Long id){
		return this.fornecedorRepositorio.findOne(id);
	}
	
	public void excluirFornecedor(Fornecedor fornecedor){
		this.fornecedorRepositorio.delete(fornecedor);
	}
	
	public List<Fornecedor> buscaFornecedorFantasia(String fantasia){
		return this.fornecedorRepositorio.buscaPelaFantasia(fantasia);
	}
	
	public List<Fornecedor> buscaFornecedorCnpj(String cnpjCpf){
		return this.fornecedorRepositorio.buscaPeloCnpjCpf(cnpjCpf);				
	}
}

