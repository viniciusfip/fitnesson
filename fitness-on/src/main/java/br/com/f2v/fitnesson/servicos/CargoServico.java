package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Cargo;
import br.com.f2v.fitnesson.repositorios.CargoRepositorio;

@Service
public class CargoServico {

	@Inject
	private CargoRepositorio cargoRepositorio;
	
	public Cargo salvar(Cargo cargo){
		return this.cargoRepositorio.save(cargo);
	}
	
	public List<Cargo> listarTodos(){
		return this.cargoRepositorio.findAll();
	}
	
	public List<Cargo> listarCargoNome(String nome){
		return this.cargoRepositorio.listaCargoNome(nome);
	}
	
	public void ExcluirCargo(Cargo cargo){
		cargoRepositorio.delete(cargo);
	}
}
