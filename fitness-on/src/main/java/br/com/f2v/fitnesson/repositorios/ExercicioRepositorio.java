package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.Exercicio;



@Transactional
public interface ExercicioRepositorio extends JpaRepository<Exercicio, Long>, 
JpaSpecificationExecutor<Exercicio> {

	@Query("SELECT e FROM Exercicio e where e.nome LIKE %:nome% ORDER BY e.nome ASC")
	public List<Exercicio> listaGrupoNome(@Param("nome") String nome);
}
