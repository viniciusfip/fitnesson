package br.com.f2v.fitnesson.servicos;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.CaixaMovimento;
import br.com.f2v.fitnesson.repositorios.CaixaMovimentoRepositorio;

@Service
public class CaixaMovimentoServico {
	
	@Inject
	private CaixaMovimentoRepositorio caixaMovimentoRepositorio;
	
	public CaixaMovimento salvar(CaixaMovimento caixaMovimento){
		return this.caixaMovimentoRepositorio.save(caixaMovimento);
	}
	
	public List<CaixaMovimento> listarMovimentoDiario(){
		return this.caixaMovimentoRepositorio.findAll();
	}
	
	public List<CaixaMovimento> listarMovimentacaoNoIntervalo(Date dataInicial,Date dataFinal){
		return this.caixaMovimentoRepositorio.buscaCaixaDiarioNoIntervalo(dataInicial, dataFinal);
	}

}
