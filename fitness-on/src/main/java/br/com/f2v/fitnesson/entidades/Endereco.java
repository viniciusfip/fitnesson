package br.com.f2v.fitnesson.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 
 * Classe que representa a entidade Endereco
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */

@Table
@Entity
@SequenceGenerator(name="GEN_ENDERECO_ID", sequenceName="SEQ_ENDERECO_ID", allocationSize=1)
@DynamicInsert
@DynamicUpdate
@Component
@Scope("prototype")
public class Endereco {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GEN_ENDERECO_ID")
	private Long id;
	
	private String cep;
	
	private String logradouro;

	private String bairro;
	
	private String municipio; 

	private String uf;

	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getCep() {
		return this.cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return this.logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return this.bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getMunicipio() {
		return this.municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUf() {
		return this.uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	
}