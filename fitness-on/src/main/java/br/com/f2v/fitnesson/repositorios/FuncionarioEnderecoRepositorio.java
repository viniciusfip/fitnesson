package br.com.f2v.fitnesson.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.FuncionarioEndereco;

@Transactional
public interface FuncionarioEnderecoRepositorio extends JpaRepository<FuncionarioEndereco, Long>,
		JpaSpecificationExecutor<FuncionarioEndereco> {

}
