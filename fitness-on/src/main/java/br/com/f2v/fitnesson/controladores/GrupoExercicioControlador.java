package br.com.f2v.fitnesson.controladores;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.GrupoExercicio;
import br.com.f2v.fitnesson.servicos.GrupoExercicioServico;

@ManagedBean
@ViewScoped
@Component
public class GrupoExercicioControlador {

	@Inject
	private GrupoExercicioServico grupoExercicioServico;
	@Inject
	private GrupoExercicio grupoExercicio;
	
	private String textoPesquisaGrupo;
	
	private List<GrupoExercicio> grupos = new ArrayList<GrupoExercicio>();
	
	@PostConstruct
	public void init() {		
	}
	
	public GrupoExercicio getGrupoExercicio() {
		return grupoExercicio;
	}

	public void setGrupoExercicio(GrupoExercicio grupoExercicio) {
		this.grupoExercicio = grupoExercicio;
	}
	
	public String getTextoPesquisaGrupo() {
		return textoPesquisaGrupo;
	}
	
	public void setTextoPesquisaGrupo(String textoPesquisaGrupo) {
		this.textoPesquisaGrupo = textoPesquisaGrupo;
	}
	
	public void salvar(){
		grupoExercicioServico.salvarGrupoExercicio(grupoExercicio);
		setGrupoExercicio( new GrupoExercicio());
	}
	
	public List<GrupoExercicio> listarGrupos(){
		if (textoPesquisaGrupo != null) {
			grupos = grupoExercicioServico.buscarGrupoNome(textoPesquisaGrupo);
		}else {
			grupos = grupoExercicioServico.listarTodos();
		}
		
		return grupos;
	}
	
	public String chamaTelaPesquisa(){
		return "pesquisarGrupo";
	}
	
	public String alterarGrupo(GrupoExercicio grupoExercico){
		this.grupoExercicio = grupoExercico;
		return "grupoExercicio.xhtml";
	}
	
	public void excluir(GrupoExercicio grupoExercicio){
		grupoExercicioServico.excluirGrupo(grupoExercicio);
	}
	
	
	
	
}
