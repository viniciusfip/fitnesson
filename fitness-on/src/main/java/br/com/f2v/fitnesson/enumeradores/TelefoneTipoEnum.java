package br.com.f2v.fitnesson.enumeradores;


/**
 * 
 * Classe que representa as constante de tipo de teleone
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
public enum TelefoneTipoEnum {
	FIXO, MOVEL;
}
