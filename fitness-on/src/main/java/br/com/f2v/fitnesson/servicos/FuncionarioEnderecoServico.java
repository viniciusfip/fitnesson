package br.com.f2v.fitnesson.servicos;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.FuncionarioEndereco;
import br.com.f2v.fitnesson.repositorios.FuncionarioEnderecoRepositorio;

@Service
public class FuncionarioEnderecoServico {
	
	@Inject 
	private FuncionarioEnderecoRepositorio funcionarioEnderecoRepositorio;
	
	public void salvar(FuncionarioEndereco funcionarioEndereco){
		funcionarioEnderecoRepositorio.save(funcionarioEndereco);
	}

}
