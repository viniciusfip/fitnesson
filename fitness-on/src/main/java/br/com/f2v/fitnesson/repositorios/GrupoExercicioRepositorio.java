package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.f2v.fitnesson.entidades.GrupoExercicio;



public interface GrupoExercicioRepositorio extends JpaRepository<GrupoExercicio, Long>, 
	JpaSpecificationExecutor<GrupoExercicio> {
	
	@Query("SELECT g FROM GrupoExercicio g where g.nome LIKE %:nome% ")
	public List<GrupoExercicio> listaGrupoNome(@Param("nome") String nome);
	
	
	

}
