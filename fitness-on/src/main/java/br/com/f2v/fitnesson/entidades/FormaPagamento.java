package br.com.f2v.fitnesson.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.enumeradores.EntraNoCaixaEnum;


@Component
@Scope("prototype")
@Entity
public class FormaPagamento {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id;	
	
	private String nome;	
	
	private String observacao;	
	
	private EntraNoCaixaEnum entraNoCaixa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public EntraNoCaixaEnum getEntraNoCaixa() {
		return entraNoCaixa;
	}

	public void setEntraNoCaixa(EntraNoCaixaEnum entraNoCaixa) {
		this.entraNoCaixa = entraNoCaixa;
	}
	
	

}
