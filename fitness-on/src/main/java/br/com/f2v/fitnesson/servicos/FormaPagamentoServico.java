package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.FormaPagamento;
import br.com.f2v.fitnesson.repositorios.FormaPagamentoRepositorio;

@Service
public class FormaPagamentoServico {
	@Inject
	private FormaPagamentoRepositorio formaPagamentoRepositorio;
	
	public FormaPagamento salvar(FormaPagamento formaPagamento){
		return formaPagamentoRepositorio.save(formaPagamento);
	}
	
	public List<FormaPagamento> listarTodos(){
		return formaPagamentoRepositorio.findAll();
	}
	
	public FormaPagamento buscarPorId(Long id){
		return formaPagamentoRepositorio.findOne(id);
	}
	
	public List<FormaPagamento> listarNome(String nome){
		return this.formaPagamentoRepositorio.listarPeloNome(nome);
	}
	
	public void excluir(FormaPagamento  formapagamento){
		this.formaPagamentoRepositorio.delete(formapagamento);
	}
}
