package br.com.f2v.fitnesson.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.ContaAPagar;

@Transactional
public interface ContaAPagarRepositorio extends JpaRepository<ContaAPagar, Long>,
		JpaSpecificationExecutor<ContaAPagar> {

}
