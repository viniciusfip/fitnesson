package br.com.f2v.fitnesson.entidades.transientes;

import br.com.f2v.fitnesson.enumeradores.TelefoneTipoEnum;

/**
 * 
 * Classe que representa a interface de telefone
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
public interface TelefoneInterface {
	
	public Long getId();
	public void setId(Long id);
	
	public TelefoneTipoEnum getTelefoneTipo();
	public void setTelefoneTipo(TelefoneTipoEnum telefoneTipo);
	
	public String getNumero();
	public void setNumero(String numero);
}
