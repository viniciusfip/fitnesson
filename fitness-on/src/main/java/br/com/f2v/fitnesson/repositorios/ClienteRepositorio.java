package br.com.f2v.fitnesson.repositorios;


import java.util.List;

import javax.inject.Inject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.Cliente;

@Transactional
public interface ClienteRepositorio 
	extends JpaRepository<Cliente, Long>, 
	JpaSpecificationExecutor<Cliente>{ 
	
	
	
	@Query("SELECT c FROM Cliente c where c.id = :id")
	public List<Cliente> buscaClienteID(@Param("id") Long id);
	
	
	@Query("SELECT c FROM Cliente c where c.nome LIKE %:nome% ")
	public List<Cliente> buscaPeloNome(@Param("nome") String nome);

	@Query("SELECT c FROM Cliente c where c.numMatricula LIKE :numMatricula%")
	public List<Cliente> buscaPelaMatricula(@Param("numMatricula") String numMatricula);
	
	@Query("SELECT c FROM Cliente c where c.cpf LIKE %:cpf")
	public List<Cliente> buscaPeloCpf(@Param("cpf") String cpf);
	
	@Query("SELECT c FROM Cliente c where c.cpf LIKE %:cpf")
	public Cliente buscaCpf(@Param("cpf") String cpf);
}
