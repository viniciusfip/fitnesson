package br.com.f2v.fitnesson.entidades;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.transientes.Bean;

@Entity
@Table
@Component
@Scope("prototype")
public class Exercicio implements Bean{
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	@ManyToOne
	private GrupoExercicio grupo;
	
	
	
	public GrupoExercicio getGrupo() {
		return grupo;
	}

	public void setGrupo(GrupoExercicio grupo) {
		this.grupo = grupo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	



	
	
	
	
	
	

}
