package br.com.f2v.fitnesson.repositorios;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.CaixaMovimento;


@Transactional
public interface CaixaMovimentoRepositorio extends JpaRepository<CaixaMovimento, Long>,
		JpaSpecificationExecutor<CaixaMovimento> {
	
	@Query("SELECT c FROM CaixaMovimento c where c.dataTransacao BETWEEN :dataInicial AND :dataFinal")
	public List<CaixaMovimento> buscaCaixaDiarioNoIntervalo(@Param("dataInicial") Date dataInicial,@Param("dataFinal") Date dataFinal);

}
