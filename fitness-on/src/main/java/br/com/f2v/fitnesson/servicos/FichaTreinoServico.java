package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;


import br.com.f2v.fitnesson.entidades.FichaTreino;
import br.com.f2v.fitnesson.repositorios.FichaTreinoRepositorio;

@Service
public class FichaTreinoServico {

	@Inject
	private FichaTreinoRepositorio fichaTreinoRepositorio;
	
	public FichaTreino salvarFichaTreino(FichaTreino fichaTreino){
		return this.fichaTreinoRepositorio.save(fichaTreino);
	}
	
	public FichaTreino buscarPorId(long id){
		return this.fichaTreinoRepositorio.findOne(id);
	}
	
	public List<FichaTreino> pesquisaAlunoNome(String nome){
		return fichaTreinoRepositorio.buscaPeloNomeAluno(nome);
	}
	
	public List<FichaTreino> pesquisaAlunoMatricula(String numMatricula){
		return fichaTreinoRepositorio.buscaPelaMatricula(numMatricula);		
	}
	
	public List<FichaTreino> pesquisaAlunoCpf(String cpf){
		return fichaTreinoRepositorio.buscaPeloCpf(cpf);		
	}
	
	public FichaTreino pesquisaFichaAndamento(String cpf){
		return fichaTreinoRepositorio.buscaFichaAndamento(cpf);
	}
	
	public List<FichaTreino> listarTodas(){
		return fichaTreinoRepositorio.findAll();
	}
}
