package br.com.f2v.fitnesson.entidades;

import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.enumeradores.EstadoCivilEnum;
import br.com.f2v.fitnesson.enumeradores.StatusClienteEnum;

/**
 * 
 * Classe que representa a entidade cliente
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Component
@Scope("prototype")
public class Cliente extends Pessoa {
	
	private StatusClienteEnum statusClienteEnum;
	
	private String numMatricula;

	private String profissao;
	
	private EstadoCivilEnum estadoCivil;
	
	private Date dataCadastro;
	
	private Date aniversario;
	
	private String telefone1;
	
	private String telefone2;
	
	@Lob
	private byte[] foto;

	@OneToMany(cascade = {CascadeType.DETACH, 
			CascadeType.MERGE, 
			CascadeType.REFRESH, 
			CascadeType.PERSIST}, 
			mappedBy="cliente", 
			fetch=FetchType.LAZY)
	private Set<ClienteTelefone> telefones;
	
	@OneToMany(cascade = {CascadeType.ALL}, 
			mappedBy="cliente", 
			fetch=FetchType.LAZY)
	private Set<ClienteEndereco> enderecos;
	

	public StatusClienteEnum getStatusClienteEnum() {
		return statusClienteEnum;
	}
	public void setStatusClienteEnum(StatusClienteEnum statusClienteEnum) {
		this.statusClienteEnum = statusClienteEnum;
	}
	
	public String getProfissao() {
		return this.profissao;
	}
	
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public EstadoCivilEnum getEstadoCivil() {
		return this.estadoCivil;
	}
	public void setEstadoCivil(EstadoCivilEnum estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public byte[] getFoto() {
		return this.foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public Set<ClienteTelefone> getTelefones() {
		return this.telefones;
	}
	public void setTelefones(Set<ClienteTelefone> telefones) {
		this.telefones = telefones;
	}

	public Set<ClienteEndereco> getEnderecos() {
		return this.enderecos;
	}
	public void setEnderecos(Set<ClienteEndereco> enderecos) {
		this.enderecos = enderecos;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public String getNumMatricula() {
		return numMatricula;
	}
	public void setNumMatricula(String numMatricula) {
		this.numMatricula = numMatricula;
	}
	public Date getAniversario() {
		return aniversario;
	}
	public void setAniversario(Date aniversario) {
		this.aniversario = aniversario;
	}
	public String getTelefone1() {
		return telefone1;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public String getTelefone2() {
		return telefone2;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	
	
	

}