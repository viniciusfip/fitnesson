package br.com.f2v.fitnesson.entidades;


import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NotFound;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.enumeradores.SituacaoContaAPagarEnum;




@Component
@Scope("prototype")
@Entity
public class ContaAPagar {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id;
		
	private Date dataCadastro;
	
	//@NotNull(message ="Informe a data de vencimento")
	private Date dataVencimento;
	
	private double valorTotal;
	
	@Inject
	@OneToOne
	private Fornecedor fornecedor;
	
	@Inject
	@OneToOne
	private FormaPagamento formapagamento;
	
	private String numDocumento;
	
	private double desconto;
	
	private double juros;
	
	private double valorPagar;
	
	private double valorPago;
	
	private Date dataPagamento;
	
	private String obs;
	
	private String historico;
	
	private SituacaoContaAPagarEnum situacaoContaPagar;
	
	public ContaAPagar(){
		fornecedor = new Fornecedor();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getNumDocumento() {
		return numDocumento;
	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	public double getDesconto() {
		return desconto;
	}

	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}

	public double getJuros() {
		return juros;
	}

	public void setJuros(double juros) {
		this.juros = juros;
	}

	public double getValorPagar() {
		return valorPagar;
	}

	public void setValorPagar(double valorPagar) {
		this.valorPagar = valorPagar;
	}

	public double getValorPago() {
		return valorPago;
	}

	public void setValorPago(double valorPago) {
		this.valorPago = valorPago;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public SituacaoContaAPagarEnum getSituacaoContaPagar() {
		return situacaoContaPagar;
	}

	public void setSituacaoContaPagar(SituacaoContaAPagarEnum situacaoContaPagar) {
		this.situacaoContaPagar = situacaoContaPagar;
	}
	
	public FormaPagamento getFormapagamento() {
		return formapagamento;
	}

	public void setFormapagamento(FormaPagamento formapagamento) {
		this.formapagamento = formapagamento;
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	
	

}
