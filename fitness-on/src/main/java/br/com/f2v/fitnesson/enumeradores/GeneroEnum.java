package br.com.f2v.fitnesson.enumeradores;

/**
 * 
 * Classe que representa as constantes de genero sexual
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
public enum GeneroEnum {
	FEMININO, MASCULINO;
}
