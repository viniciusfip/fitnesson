package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Perimetria;
import br.com.f2v.fitnesson.repositorios.PerimetriaRepositorio;

@Service
public class PerimetriaServico {
	
	@Inject 
	private PerimetriaRepositorio perimetriaRepositorio;
	
	public void salvarPerimetria(Perimetria perimetria){
		this.perimetriaRepositorio.save(perimetria);
	}

	public List<Perimetria> listarTodos(){
		return this.perimetriaRepositorio.findAll();
	}
	
	public Perimetria buscarPorId(long id){
		return this.perimetriaRepositorio.findOne(id);
	}
}
