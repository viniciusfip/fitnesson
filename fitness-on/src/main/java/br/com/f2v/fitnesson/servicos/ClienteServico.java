package br.com.f2v.fitnesson.servicos;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Cliente;
import br.com.f2v.fitnesson.repositorios.ClienteRepositorio;

@Service
public class ClienteServico {

	@Inject
	private ClienteRepositorio clienteRepositorio;
	
	public Cliente salvar(Cliente cliente) {
		return this.clienteRepositorio.save(cliente);
	}
	
	public Cliente recuperaClienteID(Long id){
		return this.clienteRepositorio.findOne(id);
	}
	
	public List<Cliente> recuperaListaPeloId(Long id){
		return this.clienteRepositorio.buscaClienteID(id);
	}
	
	public List<Cliente> listaTodosAlunos(){
		return clienteRepositorio.findAll();
	}
	
	public List<Cliente> buscaAlunos(String nome){
		return clienteRepositorio.buscaPeloNome(nome);		
	}
	
	public List<Cliente> buscaAlunosMatricula(String matricula){
		return clienteRepositorio.buscaPelaMatricula(matricula);
	}
	
	/*
	 * retorna uma lista pra poder exibir o resultado no Datatable que so retorna List
	 */
	public List<Cliente> buscaAlunoCpf(String cpf){
		return clienteRepositorio.buscaPeloCpf(cpf);
	}
	
	/*
	 * 
	 */
	public Cliente pesquisaPeloCpf(String cpf){
		return clienteRepositorio.buscaCpf(cpf);
	}
	
	public void excluir(Cliente cliente){
		clienteRepositorio.delete(cliente);
	}
	
}
