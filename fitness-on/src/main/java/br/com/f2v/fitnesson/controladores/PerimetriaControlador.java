package br.com.f2v.fitnesson.controladores;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Perimetria;
import br.com.f2v.fitnesson.servicos.PerimetriaServico;

@ManagedBean
@ViewScoped
@Component
public class PerimetriaControlador {
	
	@Inject
	private Perimetria perimetria;
	
	@Inject  
	private PerimetriaServico perimetriaServico;

	public Perimetria getPerimetria() {
		return perimetria;
	}

	public void setPerimetria(Perimetria perimetria) {
		this.perimetria = perimetria;
	}
	
	
	public void salvar(){
		perimetriaServico.salvarPerimetria(perimetria);
		setPerimetria(new Perimetria());
	}

}
