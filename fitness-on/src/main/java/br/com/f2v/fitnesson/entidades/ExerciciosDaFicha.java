package br.com.f2v.fitnesson.entidades;

import java.util.Set;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Table
@Component
@Scope("prototype")
public class ExerciciosDaFicha {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Inject
	@OneToOne
	private Exercicio exercicio;

	@Inject
	@OneToOne
	private Cliente cliente;
	
	@Inject
	@OneToOne
	private FichaTreino fichaTreino;
	
	@Inject
	@OneToOne
	private TipoTreino tipoTreino;
	
	private byte posicao;
	
	private byte serie;
	
	private short carga;
	
	private short repeticoes;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

	public FichaTreino getFichaTreino() {
		return fichaTreino;
	}

	public void setFichaTreino(FichaTreino fichaTreino) {
		this.fichaTreino = fichaTreino;
	}

	public Exercicio getExercicio() {
		return exercicio;
	}

	public void setExercicio(Exercicio exercicio) {
		this.exercicio = exercicio;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	

	public TipoTreino getTipoTreino() {
		return tipoTreino;
	}

	public void setTipoTreino(TipoTreino tipoTreino) {
		this.tipoTreino = tipoTreino;
	}

	public byte getPosicao() {
		return posicao;
	}

	public void setPosicao(byte posicao) {
		this.posicao = posicao;
	}

	public byte getSerie() {
		return serie;
	}
	
	public void setSerie(byte serie) {
		this.serie = serie;
	}
	
	public short getRepeticoes() {
		return repeticoes;
	}

	public void setRepeticoes(short repeticoes) {
		this.repeticoes = repeticoes;
	}

	public short getCarga() {
		return carga;
	}

	public void setCarga(short carga) {
		this.carga = carga;
	}

	
	

}
