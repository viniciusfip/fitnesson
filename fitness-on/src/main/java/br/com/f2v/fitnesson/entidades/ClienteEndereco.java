package br.com.f2v.fitnesson.entidades;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.transientes.Bean;
import br.com.f2v.fitnesson.enumeradores.EnderecoTipoEnum;

/**
 * 
 * Classe que representa a entidade ClienteEndereco 
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
@Entity
@Table
@Component
@Scope("prototype")
public class ClienteEndereco implements Bean {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private EnderecoTipoEnum tipo;
	
	private String complemento;
	
	private String numero;

	@Inject
	@OneToOne(cascade = {CascadeType.DETACH, 
			CascadeType.MERGE, 
			CascadeType.REFRESH,			
			CascadeType.PERSIST})
	private Cliente cliente;

	@Inject
	@OneToOne(cascade = {CascadeType.DETACH, 
			CascadeType.MERGE, 
			CascadeType.REFRESH,
			CascadeType.PERSIST})
	private Endereco endereco;
	

	public ClienteEndereco(){
		endereco = new Endereco();
	}
	
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public EnderecoTipoEnum getTipo() {
		return this.tipo;
	}
	public void setTipo(EnderecoTipoEnum tipo) {
		this.tipo = tipo;
	}

	public String getComplemento() {
		return this.complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getNumero() {
		return this.numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Endereco getEndereco() {
		return this.endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
}