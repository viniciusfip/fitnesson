package br.com.f2v.fitnesson.controladores;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.CaixaMovimento;
import br.com.f2v.fitnesson.entidades.ContaAPagar;
import br.com.f2v.fitnesson.entidades.FormaPagamento;
import br.com.f2v.fitnesson.entidades.Fornecedor;
import br.com.f2v.fitnesson.enumeradores.EntraNoCaixaEnum;
import br.com.f2v.fitnesson.enumeradores.SituacaoContaAPagarEnum;
import br.com.f2v.fitnesson.enumeradores.SituacaoContaAReceberEnum;
import br.com.f2v.fitnesson.servicos.CaixaMovimentoServico;
import br.com.f2v.fitnesson.servicos.ContaAPagarServico;
import br.com.f2v.fitnesson.servicos.FormaPagamentoServico;
import br.com.f2v.fitnesson.servicos.FornecedorServico;

@ManagedBean
@ViewScoped
@Component
public class ContaAPagarControlador {

	
	@Inject
	private ContaAPagar contaAPagar;
	
	@Inject
	private Fornecedor fornecedor;
	
	@Inject
	private FormaPagamento formaPagamento;

	@Inject
	private FormaPagamentoServico formaPagamentoServico;
	
	@Inject
	private ContaAPagarServico contaAPagarServico;
	
	@Inject
	private CaixaMovimentoServico caixaMovimentoServico;
	
	@Inject
	private FornecedorServico fornecedorServico;
	
	private SituacaoContaAPagarEnum situacaoContaAPagarEnum;
	
	private long fornecedorId;
	
	private long formaPagamentoId;	
	

	public ContaAPagar getContaAPagar() {
		return contaAPagar;
	}

	public void setContaAPagar(ContaAPagar contaAPagar) {
		this.contaAPagar = contaAPagar;
	}
	
	public FornecedorServico getFornecedorServico() {
		return fornecedorServico;
	}
			
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public FormaPagamentoServico getFormaPagamentoServico() {
		return formaPagamentoServico;
	}

	public void setFormaPagamentoServico(FormaPagamentoServico formaPagamentoServico) {
		this.formaPagamentoServico = formaPagamentoServico;
	}

	public ContaAPagarServico getContaAPagarServico() {
		return contaAPagarServico;
	}

	public void setContaAPagarServico(ContaAPagarServico contaAPagarServico) {
		this.contaAPagarServico = contaAPagarServico;
	}

	public long getFormaPagamentoId() {
		return formaPagamentoId;
	}

	public void setFormaPagamentoId(long formaPagamentoId) {
		this.formaPagamentoId = formaPagamentoId;
	}

	public long getFornecedorId() {
		return fornecedorId;
	}

	public void setFornecedorId(long fornecedorId) {
		this.fornecedorId = fornecedorId;
	}

	public void setFornecedorServico(FornecedorServico fornecedorServico) {
		this.fornecedorServico = fornecedorServico;
	}
	
	public List<FormaPagamento> listarTodasFormasPagamento() {
		return formaPagamentoServico.listarTodos();
	}

	public SituacaoContaAPagarEnum[] getSituacaoContaAPagarEnums() {
		return SituacaoContaAPagarEnum.values();
	}

	public void setSituacaoContaAPagarEnum(SituacaoContaAPagarEnum situacaoContaAPagarEnum) {
		this.situacaoContaAPagarEnum = situacaoContaAPagarEnum;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public List<Fornecedor> listarFornecedores(){
		return fornecedorServico.listarTodos();
	}
	
	public void salvar(){
		
		Fornecedor fornecedorSelecionado = fornecedorServico.buscaPorId(fornecedorId);
		FormaPagamento formaSelecionada = formaPagamentoServico.buscarPorId(formaPagamentoId);
		contaAPagar.setFornecedor(fornecedorSelecionado);
		contaAPagar.setFormapagamento(formaSelecionada);		
		contaAPagarServico.salvar(contaAPagar);
		//novaConta();
	}
	
	public void novaConta(){
		setFornecedor(new Fornecedor());
		setContaAPagar(new ContaAPagar());
	}
	
	public void calculaValorAPagarComDesconto() {
		double resultado;
		if(this.contaAPagar.getDesconto()>0){			
			contaAPagar.setJuros(0.0);			
			resultado = contaAPagar.getValorTotal() - contaAPagar.getDesconto();			
			contaAPagar.setValorPagar(resultado);
		}		
	}
	
	public void calculaValorAPagarComJuros() {
		double resultado;		
		if(this.contaAPagar.getJuros()>0){			
			contaAPagar.setDesconto(0.0);
			resultado = contaAPagar.getValorTotal() + contaAPagar.getJuros();			
			contaAPagar.setValorPagar(resultado);
		}
	}

	public void calculaValorPagarcomValorTotal(){		
		contaAPagar.setDesconto(0.0);		
		contaAPagar.setJuros(0.0);	
		contaAPagar.setValorPagar(contaAPagar.getValorTotal());		
	}	
	
	public void liquidaConta() {

		
		contaAPagar.setSituacaoContaPagar(SituacaoContaAPagarEnum.QUITADA);
		contaAPagarServico.salvar(contaAPagar);
		
	    
		FormaPagamento formaSelecionada = formaPagamentoServico.buscarPorId(formaPagamentoId);

		// Se no cadastra forma de pagamento o entra no caixa estiver como
		// SIM. Insere no caixa diario

		if (formaSelecionada.getEntraNoCaixa() == EntraNoCaixaEnum.SIM) {
			CaixaMovimento caixaMovimento = new CaixaMovimento();

			Date hoje = new Date();
			caixaMovimento.setDataTransacao(hoje);

			caixaMovimento.setValorDebito(contaAPagar.getValorPago());			

			caixaMovimento.setRecebeIdConta(contaAPagar.getId());
			caixaMovimento.setFormaPagamento(formaSelecionada);
			
			
			
			
			caixaMovimentoServico.salvar(caixaMovimento);
		}

	}


	
	
	
}
