package br.com.f2v.fitnesson.controladores;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.FornecedorEndereco;
import br.com.f2v.fitnesson.servicos.FornecedorEnderecoServico;
import br.com.f2v.fitnesson.servicos.FornecedorServico;


@ManagedBean
@ViewScoped
@Component
public class FornecedorEnderecoControlador {
	
	@Inject
	private FornecedorEnderecoServico fornecedorEnderecoServico;
	
	@Inject
	private FornecedorEndereco fornecedorEndereco;

	
	public FornecedorEnderecoServico getFornecedorEnderecoServico() {
		return fornecedorEnderecoServico;
	}


	public void setFornecedorEnderecoServico(
			FornecedorEnderecoServico fornecedorEnderecoServico) {
		this.fornecedorEnderecoServico = fornecedorEnderecoServico;
	}


	public void salvarFornecedorEndereco(){
		fornecedorEnderecoServico.salvar(fornecedorEndereco);
	}
	
	
	

}
