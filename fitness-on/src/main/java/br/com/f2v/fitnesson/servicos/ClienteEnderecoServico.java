package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.ClienteEndereco;
import br.com.f2v.fitnesson.repositorios.ClienteEnderecoRepositorio;
import br.com.f2v.fitnesson.repositorios.ClienteRepositorio;

@Service
public class ClienteEnderecoServico {
	
	@Inject
	private ClienteEnderecoRepositorio clienteEnderecoRepositorio;
	
	
	public ClienteEndereco salvar(ClienteEndereco clienteEndereco) {
		return this.clienteEnderecoRepositorio.save(clienteEndereco);		
	} 
	
	public void excluir(ClienteEndereco clienteEndereco){
		this.clienteEnderecoRepositorio.delete(clienteEndereco);
	}
	
	public List<ClienteEndereco> pesquisaAlunoNome(String nome){
		return clienteEnderecoRepositorio.buscaPeloNomeAluno(nome);
	}
	
	public List<ClienteEndereco> pesquisaAlunoMatricula(String numMatricula){
		return clienteEnderecoRepositorio.buscaPelaMatricula(numMatricula);		
	}
	
	public List<ClienteEndereco> pesquisaAlunoCpf(String cpf){
		return clienteEnderecoRepositorio.buscaPeloCpf(cpf);		
	}
	
	
	
}
