package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.Cliente;
import br.com.f2v.fitnesson.entidades.ClienteEndereco;


@Transactional
public interface ClienteEnderecoRepositorio extends JpaRepository<ClienteEndereco, Long>, 
	JpaSpecificationExecutor<ClienteEndereco>{
	
	
	
	@Query("SELECT c FROM ClienteEndereco c where c.cliente.nome LIKE %:nome% ")
	public List<ClienteEndereco> buscaPeloNomeAluno(@Param("nome") String nome);
	
	@Query("SELECT c FROM ClienteEndereco c where c.cliente.numMatricula LIKE :numMatricula%")
	public List<ClienteEndereco> buscaPelaMatricula(@Param("numMatricula") String numMatricula);
	
	@Query("SELECT c FROM ClienteEndereco c where c.cliente.cpf LIKE :cpf")
	public List<ClienteEndereco> buscaPeloCpf(@Param("cpf") String cpf);

}
