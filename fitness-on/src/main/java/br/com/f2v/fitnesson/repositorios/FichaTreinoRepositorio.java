package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import br.com.f2v.fitnesson.entidades.FichaTreino;

public interface FichaTreinoRepositorio extends JpaRepository<FichaTreino, Long>,
		JpaSpecificationExecutor<FichaTreino> {
	
	@Query("SELECT f FROM FichaTreino f where f.cliente.nome LIKE %:nome% ")
	public List<FichaTreino> buscaPeloNomeAluno(@Param("nome") String nome);
	
	@Query("SELECT f FROM FichaTreino f where f.cliente.numMatricula LIKE :numMatricula%")
	public List<FichaTreino> buscaPelaMatricula(@Param("numMatricula") String numMatricula);
	
	@Query("SELECT f FROM FichaTreino f where f.cliente.cpf LIKE :cpf")
	public List<FichaTreino> buscaPeloCpf(@Param("cpf") String cpf);
	
	@Query("SELECT f FROM FichaTreino f where f.fichaEnum =0 AND f.cliente.cpf LIKE :cpf")
	public FichaTreino buscaFichaAndamento(@Param("cpf") String cpf);

}
