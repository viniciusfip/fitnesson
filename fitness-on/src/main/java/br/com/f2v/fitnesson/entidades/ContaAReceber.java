package br.com.f2v.fitnesson.entidades;

import java.time.LocalDate;
import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.enumeradores.SituacaoContaAPagarEnum;
import br.com.f2v.fitnesson.enumeradores.SituacaoContaAReceberEnum;

@Component
@Scope("prototype")
@Entity
public class ContaAReceber {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Date vencimento;

	private int numeroParcelas;

	private double valorTotal;

	private Date dataCadastroParcela;

	private double valorParcela;

	private double desconto;
	
	private double juros;

	private double valorReceber;

	private double valorRecebido;

	private SituacaoContaAReceberEnum situacaoConta;

	private Date dataPagamentoParcela;

	
	@Inject
	@OneToOne
	private Cliente cliente;

	 @Inject
	 @OneToOne
	 private FormaPagamento formaPagamentoParcela;

	private String obs;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public int getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(int numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Date getDataCadastroParcela() {
		return dataCadastroParcela;
	}

	public void setDataCadastroParcela(Date dataCadastroParcela) {
		this.dataCadastroParcela = dataCadastroParcela;
	}

	public double getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(double valorParcela) {
		this.valorParcela = valorParcela;
	}

	public double getDesconto() {
		return desconto;
	}

	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}	

	public double getJuros() {
		return juros;
	}

	public void setJuros(double juros) {
		this.juros = juros;
	}

	public void setValorReceber(double valorReceber) {
		this.valorReceber = valorReceber;
	}

	public double getValorReceber() {
		return valorReceber;
	}
	
	public FormaPagamento getFormaPagamentoParcela() {
		return formaPagamentoParcela;
	}

	public void setFormaPagamentoParcela(FormaPagamento formaPagamentoParcela) {
		this.formaPagamentoParcela = formaPagamentoParcela;
	}

	public double getValorRecebido() {
		return valorRecebido;
	}

	public void setValorRecebido(double valorRecebido) {
		this.valorRecebido = valorRecebido;
	}

	
	public SituacaoContaAReceberEnum getSituacaoConta() {
		return situacaoConta;
	}

	public void setSituacaoConta(SituacaoContaAReceberEnum situacaoConta) {
		this.situacaoConta = situacaoConta;
	}

	public Date getDataPagamentoParcela() {
		return dataPagamentoParcela;
	}

	public void setDataPagamentoParcela(Date dataPagamentoParcela) {
		this.dataPagamentoParcela = dataPagamentoParcela;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	
}
