package br.com.f2v.fitnesson.controladores;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;





import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Usuario;
import br.com.f2v.fitnesson.servicos.UsuarioServico;

@ManagedBean
@SessionScoped
@Component
public class UsuarioControlador {
	
	@Inject
	private UsuarioServico usuarioServico;
	
	@Inject
	private Usuario usuario;	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
  public String logar(){
	  usuario = usuarioServico.recuperaUsuario(usuario.getLogin(), usuario.getSenha());
	  
	  if (usuario == null) {
          //usuario = new Usuario();
          FacesContext.getCurrentInstance().addMessage(
                     null,
                     new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usu�rio n�o encontrado!",
                                 "Erro no Login!"));
          return null;
    } else {
          return "/index";
    }
  }
	
	

}
