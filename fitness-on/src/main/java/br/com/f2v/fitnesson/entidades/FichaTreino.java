package br.com.f2v.fitnesson.entidades;

import java.util.Date;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.enumeradores.FichaTreinoEnum;

@Entity
@Table
@Component
@Scope("prototype")
public class FichaTreino {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Inject
	@OneToOne
	private Cliente cliente;
	
	private FichaTreinoEnum fichaEnum;
	
	private Date dataInicio;
	
	private Date alteracaoTreino;
	
	private Date fimTreino;
	
	private String objetivoTreino;
	
	private String telefoneEmergencia;
	
	private String telefone1;
	
	private String telefone2;
	
	private String observacao;
	
	private String domingo,segunda,terca,quarta,quinta,sexta,sabado;
	
	private Integer sequenciaTreino;
	
	@OneToMany(cascade = {CascadeType.ALL})
	private Set<ExerciciosDaFicha> exerciciosDaFicha;
	
	@OneToMany(cascade = {CascadeType.ALL})
	private Set<PerimetriaDaFicha> perimetriaDaFicha;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public FichaTreinoEnum getFichaEnum() {
		return fichaEnum;
	}

	public void setFichaEnum(FichaTreinoEnum fichaEnum) {
		this.fichaEnum = fichaEnum;
	}

	public Set<ExerciciosDaFicha> getExerciciosDaFicha() {
		return exerciciosDaFicha;
	}

	public void setExerciciosDaFicha(Set<ExerciciosDaFicha> exerciciosDaFicha) {
		this.exerciciosDaFicha = exerciciosDaFicha;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getAlteracaoTreino() {
		return alteracaoTreino;
	}

	public void setAlteraca0Treino(Date alteracaoTreino) {
		this.alteracaoTreino = alteracaoTreino;
	}

	public Date getFimTreino() {
		return fimTreino;
	}

	public void setFimTreino(Date fimTreino) {
		this.fimTreino = fimTreino;
	}

	public String getObjetivoTreino() {
		return objetivoTreino;
	}

	public void setObjetivoTreino(String objetivoTreino) {
		this.objetivoTreino = objetivoTreino;
	}

	public String getTelefoneEmergencia() {
		return telefoneEmergencia;
	}

	public void setTelefoneEmergencia(String telefoneEmergencia) {
		this.telefoneEmergencia = telefoneEmergencia;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Set<PerimetriaDaFicha> getPerimetriaDaFicha() {
		return perimetriaDaFicha;
	}

	public void setPerimetriaDaFicha(Set<PerimetriaDaFicha> perimetriaDaFicha) {
		this.perimetriaDaFicha = perimetriaDaFicha;
	}

	public void setAlteracaoTreino(Date alteracaoTreino) {
		this.alteracaoTreino = alteracaoTreino;
	}

	public String getDomingo() {
		return domingo;
	}

	public void setDomingo(String domingo) {
		this.domingo = domingo;
	}

	public String getSegunda() {
		return segunda;
	}

	public void setSegunda(String segunda) {
		this.segunda = segunda;
	}

	public String getTerca() {
		return terca;
	}

	public void setTerca(String terca) {
		this.terca = terca;
	}

	public String getQuarta() {
		return quarta;
	}

	public void setQuarta(String quarta) {
		this.quarta = quarta;
	}

	public String getQuinta() {
		return quinta;
	}

	public void setQuinta(String quinta) {
		this.quinta = quinta;
	}

	public String getSexta() {
		return sexta;
	}

	public void setSexta(String sexta) {
		this.sexta = sexta;
	}

	public String getSabado() {
		return sabado;
	}

	public void setSabado(String sabado) {
		this.sabado = sabado;
	}

	public Integer getSequenciaTreino() {
		return sequenciaTreino;
	}

	public void setSequenciaTreino(Integer sequenciaTreino) {
		this.sequenciaTreino = sequenciaTreino;
	}

	

	
	
}
