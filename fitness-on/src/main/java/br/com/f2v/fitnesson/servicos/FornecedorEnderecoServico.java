package br.com.f2v.fitnesson.servicos;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Fornecedor;
import br.com.f2v.fitnesson.entidades.FornecedorEndereco;
import br.com.f2v.fitnesson.repositorios.FornecedorEnderecoRepositorio;
import br.com.f2v.fitnesson.repositorios.FornecedorRepositorio;

@Service
public class FornecedorEnderecoServico {
	
	@Inject
	private FornecedorEnderecoRepositorio fornecedorEnderecoRepositorio;
	
	public FornecedorEndereco salvar(FornecedorEndereco fornecedorEndereco){
		return this.fornecedorEnderecoRepositorio.save(fornecedorEndereco);
	}

}
