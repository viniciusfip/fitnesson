package br.com.f2v.fitnesson.controladores;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Endereco;
import br.com.f2v.fitnesson.entidades.Fornecedor;
import br.com.f2v.fitnesson.entidades.FornecedorEndereco;
import br.com.f2v.fitnesson.servicos.FornecedorEnderecoServico;
import br.com.f2v.fitnesson.servicos.FornecedorServico;

@ManagedBean
@ViewScoped
@Component
public class FornecedorControlador {
	
	@Inject
	private Fornecedor fornecedor;
	
	@Inject 
	private FornecedorEndereco fornecedorEndereco;
	
	@Inject
	private FornecedorServico fornecedorServico;
	
	@Inject
	private FornecedorEnderecoServico fornecedorEnderecoServico;
	
	@Inject
	private Endereco endereco;
	
	private String textoPesquisaFornecedor;
	
	private List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
	
	private String opcaoSelectFornecedor;
	
	
	
	@PostConstruct
	public void init(){
		opcaoSelectFornecedor = "opcao";
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	

	public String getOpcaoSelectFornecedor() {
		return opcaoSelectFornecedor;
	}

	public void setOpcaoSelectFornecedor(String opcaoSelectFornecedor) {
		this.opcaoSelectFornecedor = opcaoSelectFornecedor;
	}

	public String getTextoPesquisaFornecedor() {
		return textoPesquisaFornecedor;
	}

	public void setTextoPesquisaFornecedor(String textoPesquisaFornecedor) {
		this.textoPesquisaFornecedor = textoPesquisaFornecedor;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public FornecedorEndereco getFornecedorEndereco() {
		return fornecedorEndereco;
	}

	public void setFornecedorEndereco(FornecedorEndereco fornecedorEndereco) {
		this.fornecedorEndereco = fornecedorEndereco;
	}
	
	public String salvarFornecedor(){
		fornecedor.setEnderecos(new HashSet<>());
		fornecedor.getEnderecos().add(fornecedorEndereco);

		fornecedorEndereco.setFornecedor(fornecedor);
		fornecedorEnderecoServico.salvar(fornecedorEndereco);
		novoFornecedor();
		
		return "fornecedorCadastrado";
	}
	
	public void novoFornecedor(){
		setFornecedor(new Fornecedor());
		setEndereco(new Endereco());
		setFornecedorEndereco(new FornecedorEndereco());
	}
	
	public String chamaTelaPesquisaFornecedor(){
		return "pesquisarFornecedor";
	}
	
	public String alterarFornecedor(Fornecedor fornecedor){
		this.fornecedor = fornecedor;
		return "cadastrarFornecedor.xhtml";
	}
	
	public List<Fornecedor> listarFornecedores(){		
		
		if (opcaoSelectFornecedor.equals("Razao")) {
			fornecedores = fornecedorServico.buscaFornecedorNome(textoPesquisaFornecedor);	
			
		} else if (opcaoSelectFornecedor.equals("Fantasia")) {
			fornecedores = fornecedorServico.buscaFornecedorFantasia(textoPesquisaFornecedor);
			
		} else if(opcaoSelectFornecedor.equalsIgnoreCase("cnpjCpf")){
			
			fornecedores = fornecedorServico.buscaFornecedorCnpj(textoPesquisaFornecedor);
		}else {
			fornecedores = fornecedorServico.listarTodos();
		}
		
		return fornecedores;		
	}
	
	public void deleteFornecedor(Fornecedor fornecedor){
		System.out.println("Fornecedor "+fornecedor.getNome());
		fornecedorServico.excluirFornecedor(fornecedor);
	}

}
