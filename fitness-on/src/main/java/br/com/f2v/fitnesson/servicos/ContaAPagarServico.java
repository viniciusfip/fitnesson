package br.com.f2v.fitnesson.servicos;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.ContaAPagar;
import br.com.f2v.fitnesson.repositorios.ContaAPagarRepositorio;

@Service
public class ContaAPagarServico {
	
	@Inject
	private ContaAPagarRepositorio contaAPagarRepositorio;
	
	public ContaAPagar salvar(ContaAPagar contaAPagar){
		return contaAPagarRepositorio.save(contaAPagar);
	}

}
