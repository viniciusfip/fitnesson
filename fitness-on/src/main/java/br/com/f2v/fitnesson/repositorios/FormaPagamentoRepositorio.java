package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.FormaPagamento;
@Transactional
public interface FormaPagamentoRepositorio extends JpaRepository<FormaPagamento, Long>,
		JpaSpecificationExecutor<FormaPagamento> {

	@Query("SELECT f FROM FormaPagamento f where f.nome LIKE %:nome% ORDER BY f.nome")
	public List<FormaPagamento> listarPeloNome(@Param("nome") String nome);
}
