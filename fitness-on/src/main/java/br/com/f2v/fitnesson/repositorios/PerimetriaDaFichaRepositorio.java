package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


import br.com.f2v.fitnesson.entidades.PerimetriaDaFicha;


@Transactional
public interface PerimetriaDaFichaRepositorio  extends JpaRepository<PerimetriaDaFicha, Long>,
JpaSpecificationExecutor<PerimetriaDaFicha> {
	
	@Query("SELECT p FROM PerimetriaDaFicha p where p.fichaTreino.id = :idFicha and p.cliente.id = :idCliente  ")
	public List<PerimetriaDaFicha> bucarPerimetriaDaFicha(@Param("idFicha") Long idFicha, @Param("idCliente") Long idCliente);

}
