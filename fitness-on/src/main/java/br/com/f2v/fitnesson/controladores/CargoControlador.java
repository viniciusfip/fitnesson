package br.com.f2v.fitnesson.controladores;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Cargo;
import br.com.f2v.fitnesson.servicos.CargoServico;

@ManagedBean
@ViewScoped
@Component
public class CargoControlador {
	
	@Inject
	private CargoServico cargoServico;
	
	@Inject
	private Cargo cargo;
	
	private String textoPesquisaCargo;
	
	private List<Cargo> cargos = new ArrayList<Cargo>();
	
	public void salvar(){
		cargoServico.salvar(cargo);
		setCargo(new Cargo());
	}

	public Cargo getCargo() {
		return cargo;
	}

	public String getTextoPesquisaCargo() {
		return textoPesquisaCargo;
	}

	public void setTextoPesquisaCargo(String textoPesquisaCargo) {
		this.textoPesquisaCargo = textoPesquisaCargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public String chamaTelaPesquisa(){
		return "pesquisarCargo";
	}
	
	public List<Cargo> listarCargos(){
		if(textoPesquisaCargo != null){			
			cargos = cargoServico.listarCargoNome(textoPesquisaCargo);
		}else {
			cargos = cargoServico.listarTodos();			
		}
		return cargos;		
	}
	
	public String alterarCargo(Cargo cargo){
		this.cargo = cargo;
		return "CadastraCargo.xhtml";
	}
	
	public void excluir(Cargo cargo){
		cargoServico.ExcluirCargo(cargo);
	}
	

}
