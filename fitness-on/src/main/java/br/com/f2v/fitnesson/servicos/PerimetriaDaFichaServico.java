package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.PerimetriaDaFicha;
import br.com.f2v.fitnesson.repositorios.PerimetriaDaFichaRepositorio;


@Service
public class PerimetriaDaFichaServico {
	
	@Inject
	private PerimetriaDaFichaRepositorio perimetriaRepositorio;
	
	public void salvarPerimetriaDaFicha(PerimetriaDaFicha perimetriaDaFicha){
		this.perimetriaRepositorio.save(perimetriaDaFicha);
	}

	public List<PerimetriaDaFicha> listarTodas(){
		return this.perimetriaRepositorio.findAll();		
	}
	
	public List<PerimetriaDaFicha> listarPerimetriaDaFicha(long idFicha,long idCliente){
		return this.perimetriaRepositorio.bucarPerimetriaDaFicha(idFicha, idCliente);
	}
	
}
