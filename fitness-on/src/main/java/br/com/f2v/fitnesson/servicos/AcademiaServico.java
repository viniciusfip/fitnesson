package br.com.f2v.fitnesson.servicos;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Academia;
import br.com.f2v.fitnesson.repositorios.AcademiaRepositorio;

@Service
public class AcademiaServico {

	@Inject
	private AcademiaRepositorio academiaRepositorio;
	
	public Academia salvar(Academia academia){
		return this.academiaRepositorio.save(academia);
	}
	
	public Academia listarAcademia(){
		return this.academiaRepositorio.listarAcademia();
	}
	
	
}
