package br.com.f2v.fitnesson.controladores;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.ClienteEndereco;
import br.com.f2v.fitnesson.servicos.ClienteEnderecoServico;

@ManagedBean
@ViewScoped
@Component
public class ClienteEnderecoControlador {
	
	@Inject
	private ClienteEnderecoServico clienteEnderecoServico;
	
	@Inject
	private ClienteEndereco clienteEndereco;

	public ClienteEndereco getClienteEndereco() {
		return clienteEndereco;
	}

	public void setClienteEndereco(ClienteEndereco clienteEndereco) {
		this.clienteEndereco = clienteEndereco;
	}
	
	public void salvarClienteEndereco(){
		clienteEnderecoServico.salvar(clienteEndereco);
	}
}
