package br.com.f2v.fitnesson.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.Academia;


@Transactional
public interface AcademiaRepositorio extends JpaRepository<Academia, Long>, 
	JpaSpecificationExecutor<Academia> {
	
	@Query("SELECT a FROM Academia a where a.id>0")
	public Academia listarAcademia();

}
