package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.GrupoExercicio;
import br.com.f2v.fitnesson.repositorios.GrupoExercicioRepositorio;

@Service
public class GrupoExercicioServico {
	
	@Inject
	private GrupoExercicioRepositorio grupoExercicioRepositorio;
	
	public GrupoExercicio salvarGrupoExercicio(GrupoExercicio grupoExercicio){
		return this.grupoExercicioRepositorio.save(grupoExercicio);
	}
	
	public List<GrupoExercicio> listarTodos(){
		return this.grupoExercicioRepositorio.findAll();
	}
	
	public GrupoExercicio buscarPorId(Long id){
		return this.grupoExercicioRepositorio.findOne(id);
	}

	public void excluirGrupo(GrupoExercicio grupo){
		this.grupoExercicioRepositorio.delete(grupo);
	}
	
	public List<GrupoExercicio> buscarGrupoNome(String nome){
		return grupoExercicioRepositorio.listaGrupoNome(nome);
	}
	
	
}
