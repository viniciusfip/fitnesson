package br.com.f2v.fitnesson.entidades;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Table
@Component
@Scope("prototype")
public class PerimetriaDaFicha {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Inject
	@OneToOne
	private Cliente cliente;

	@Inject
	@OneToOne
	private FichaTreino fichaTreino;

	private double peso, altura, ombro, torax, abdomem, cintura, coxaDir,
			coxaEsq, bracoDir, bracoEsq, panturilhaDir, panturilhaEsq, quadril,
			anteBracoDir, anteBracoEsq;
	
	private Date dataInicialPerimetria,dataFinalPerimetria;

	
	// @OneToOne private Perimetria perimetria;
	 

	private double medida;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	
	public Date getDataInicialPerimetria() {
		return dataInicialPerimetria;
	}


	public void setDataInicialPerimetria(Date dataInicialPerimetria) {
		this.dataInicialPerimetria = dataInicialPerimetria;
	}


	public Date getDataFinalPerimetria() {
		return dataFinalPerimetria;
	}


	public void setDataFinalPerimetria(Date dataFinalPerimetria) {
		this.dataFinalPerimetria = dataFinalPerimetria;
	}


	public FichaTreino getFichaTreino() {
		return fichaTreino;
	}


	public void setFichaTreino(FichaTreino fichaTreino) {
		this.fichaTreino = fichaTreino;
	}


	public double getPeso() {
		return peso;
	}


	public void setPeso(double peso) {
		this.peso = peso;
	}


	public double getAltura() {
		return altura;
	}


	public void setAltura(double altura) {
		this.altura = altura;
	}


	public double getOmbro() {
		return ombro;
	}


	public void setOmbro(double ombro) {
		this.ombro = ombro;
	}


	public double getTorax() {
		return torax;
	}


	public void setTorax(double torax) {
		this.torax = torax;
	}


	public double getAbdomem() {
		return abdomem;
	}


	public void setAbdomem(double abdomem) {
		this.abdomem = abdomem;
	}


	public double getCintura() {
		return cintura;
	}


	public void setCintura(double cintura) {
		this.cintura = cintura;
	}


	public double getCoxaDir() {
		return coxaDir;
	}


	public void setCoxaDir(double coxaDir) {
		this.coxaDir = coxaDir;
	}


	public double getCoxaEsq() {
		return coxaEsq;
	}


	public void setCoxaEsq(double coxaEsq) {
		this.coxaEsq = coxaEsq;
	}


	public double getBracoDir() {
		return bracoDir;
	}


	public void setBracoDir(double bracoDir) {
		this.bracoDir = bracoDir;
	}


	public double getBracoEsq() {
		return bracoEsq;
	}


	public void setBracoEsq(double bracoEsq) {
		this.bracoEsq = bracoEsq;
	}


	public double getPanturilhaDir() {
		return panturilhaDir;
	}


	public void setPanturilhaDir(double panturilhaDir) {
		this.panturilhaDir = panturilhaDir;
	}


	public double getPanturilhaEsq() {
		return panturilhaEsq;
	}


	public void setPanturilhaEsq(double panturilhaEsq) {
		this.panturilhaEsq = panturilhaEsq;
	}


	public double getQuadril() {
		return quadril;
	}


	public void setQuadril(double quadril) {
		this.quadril = quadril;
	}


	public double getAnteBracoDir() {
		return anteBracoDir;
	}


	public void setAnteBracoDir(double anteBracoDir) {
		this.anteBracoDir = anteBracoDir;
	}


	public double getAnteBracoEsq() {
		return anteBracoEsq;
	}


	public void setAnteBracoEsq(double anteBracoEsq) {
		this.anteBracoEsq = anteBracoEsq;
	}


	public double getMedida() {
		return medida;
	}


	public void setMedida(double medida) {
		this.medida = medida;
	}

	
}
