package br.com.f2v.fitnesson.enumeradores;


/**
 * 
 * Classe que representa as constantes de Tipo de endere�o
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
public enum EnderecoTipoEnum {
	COBRAN�A, COMERCIAL, RESIDENCIAL;
}
