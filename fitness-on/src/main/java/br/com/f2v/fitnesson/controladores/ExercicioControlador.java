package br.com.f2v.fitnesson.controladores;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Exercicio;
import br.com.f2v.fitnesson.entidades.GrupoExercicio;
import br.com.f2v.fitnesson.servicos.ExercicioServico;
import br.com.f2v.fitnesson.servicos.GrupoExercicioServico;

@ManagedBean
@ViewScoped
@Component
public class ExercicioControlador {
	
	@Inject
	private ExercicioServico exercicioServico;
	
	@Inject
	private Exercicio exercicio;
	
	@Inject
	private GrupoExercicioServico grupoExercicioServico;
	
	@Inject
	private GrupoExercicio grupoExercicio;
	
	private Long grupoId;
	
	private String textoPesquisaExercicio;
	
	
	private List<Exercicio> listaExercicios = new ArrayList<Exercicio>();
	private List<Exercicio> exerciciosDaPesquisa = new ArrayList<Exercicio>();
	
	private Map grupos=null;
	
	/*@PostConstruct
	public void init(){
		getGrupos();
	}*/

	public Long getGrupoId() {
		return grupoId;
	}
	public String getTextoPesquisaExercicio() {
		return textoPesquisaExercicio;
	}
	public void setTextoPesquisaExercicio(String textoPesquisaExercicio) {
		this.textoPesquisaExercicio = textoPesquisaExercicio;
	}
	public void setGrupoId(Long grupoId) {
		this.grupoId = grupoId;
	}	
	
	
	public Exercicio getExercicio() {
		return exercicio;
	}

	public void setExercicio(Exercicio exercicio) {
		this.exercicio = exercicio;
	}
	
	public GrupoExercicio getGrupoExercicio() {
		return grupoExercicio;
	}

	public void setGrupoExercicio(GrupoExercicio grupoExercicio) {
		this.grupoExercicio = grupoExercicio;
	}

	public void salvar(){		
		grupoExercicio.setExercicios(new HashSet<>());
		
		GrupoExercicio grupoSelecionado = grupoExercicioServico.buscarPorId(grupoId);
		
		grupoExercicio.getExercicios().add(exercicio);
		exercicio.setGrupo(grupoSelecionado);
		
		exercicioServico.salvarExercicio(exercicio);
	
		novoExercicio();
	}

	public void novoExercicio(){
		setExercicio(new Exercicio());
		setGrupoExercicio(new GrupoExercicio());
	}
	
	public List<GrupoExercicio> listarTodosGrupos(){
		return grupoExercicioServico.listarTodos();
	}
	
	public String chamaTelaPesquisa(){
		return "pesquisarExercicio";
	}
	
	public String alterarExercicio(Exercicio exercicio){
		this.exercicio = exercicio;
		return "cadastraExercicio.xhtml";
	}
	
	public void excluir(Exercicio exercicio){
		this.exercicioServico.excluirExercicio(exercicio);
	}
	
	public List<Exercicio> listarExercicios(){
		if (textoPesquisaExercicio != null) {
			exerciciosDaPesquisa =  exercicioServico.buscarPorNome(textoPesquisaExercicio);
		}else{
			exerciciosDaPesquisa = exercicioServico.listarTodosExercicios();
		}
		
		return exerciciosDaPesquisa;
	}
	
	/*public Map getGrupos(){
		grupos = new LinkedHashMap();
		for (Iterator iter = listarTodosGrupos().iterator(); iter.hasNext();) {
			GrupoExercicio ge = (GrupoExercicio) iter.next();
			//System.out.println(ge.getNome());
			grupos.put(ge.getNome(), ge.getNome());
		}
		return grupos;
	}*/
	
	
}
