package br.com.f2v.fitnesson.entidades;

import java.time.LocalDate;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import br.com.f2v.fitnesson.entidades.transientes.Bean;
import br.com.f2v.fitnesson.enumeradores.GeneroEnum;

/**
 * 
 * Classe abstrata que representa a entidade Pessoa
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
@MappedSuperclass
public abstract class Pessoa implements Bean {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	protected String nome;

	protected GeneroEnum genero;

	protected String cpf;

	protected String rg;

	protected LocalDate dataNascimento;

	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public GeneroEnum getGenero() {
		return this.genero;
	}
	public void setGenero(GeneroEnum genero) {
		this.genero = genero;
	}

	public String getCpf() {
		return this.cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return this.rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}

	public LocalDate getDataNascimento() {
		return this.dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
}