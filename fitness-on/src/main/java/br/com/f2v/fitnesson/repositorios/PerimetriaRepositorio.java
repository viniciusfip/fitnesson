package br.com.f2v.fitnesson.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.f2v.fitnesson.entidades.Perimetria;

public interface PerimetriaRepositorio extends JpaRepository<Perimetria, Long>,
		JpaSpecificationExecutor<Perimetria> {

}
