package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.Cargo;

@Transactional
public interface CargoRepositorio extends JpaRepository<Cargo, Long>,
		JpaSpecificationExecutor<Cargo> {
	
	@Query("SELECT c FROM Cargo c where c.nome LIKE %:nome% ")
	public List<Cargo> listaCargoNome(@Param("nome") String nome);
		
	

}
