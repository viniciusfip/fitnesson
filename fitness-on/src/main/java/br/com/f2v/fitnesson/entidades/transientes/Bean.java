package br.com.f2v.fitnesson.entidades.transientes;

/**
 * 
 * Interface para prover o identificador para todoas
 * as entidades persistiveis do sistema
 * 
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
public interface Bean {
	public Long getId();
}
