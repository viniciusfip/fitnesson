package br.com.f2v.fitnesson.controladores;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.FormaPagamento;
import br.com.f2v.fitnesson.enumeradores.EntraNoCaixaEnum;
import br.com.f2v.fitnesson.servicos.FormaPagamentoServico;

@ManagedBean
@ViewScoped
@Component
public class FormaPagamentoControlador {
	
	@Inject
	private FormaPagamento formaPagamento;
	
	@Inject
	private FormaPagamentoServico formaPagamentoServico;
	
	private EntraNoCaixaEnum entraNoCaixaEnum;
	
	private String textoPesquisaFormaPagamento;
	
	private List<FormaPagamento> todasFormasPagamento = new ArrayList<FormaPagamento>();
	private List<FormaPagamento> formasPagamentoPesquisa = new ArrayList<FormaPagamento>(); 

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	
	public String getTextoPesquisaFormaPagamento() {
		return textoPesquisaFormaPagamento;
	}

	public void setTextoPesquisaFormaPagamento(String textoPesquisaFormaPagamento) {
		this.textoPesquisaFormaPagamento = textoPesquisaFormaPagamento;
	}

	public FormaPagamentoServico getFormaPagamentoServico() {
		return formaPagamentoServico;
	}

	public void setFormaPagamentoServico(FormaPagamentoServico formaPagamentoServico) {
		this.formaPagamentoServico = formaPagamentoServico;
	}

	public EntraNoCaixaEnum[] getEntraNoCaixaEnums() {
		return EntraNoCaixaEnum.values();
	}

	public void setEntraNoCaixaEnum(EntraNoCaixaEnum entraNoCaixaEnum) {
		this.entraNoCaixaEnum = entraNoCaixaEnum;
	}
	
	public void salvar(){
		formaPagamentoServico.salvar(formaPagamento);
		novaForma();
	}
	
	public void novaForma(){
		setFormaPagamento(new FormaPagamento());
	}
	
	public String alterarFormaPagamento(FormaPagamento formaPagamento){
		this.formaPagamento = formaPagamento;
		return "formaPagamento.xhtml";
	}
	
	public String chamaTelaPesquisa(){
		return "pesquisarFormaPagamento";
	}
	
	public List<FormaPagamento> listarFormasPagamento(){
		if (textoPesquisaFormaPagamento != null) {
			formasPagamentoPesquisa = formaPagamentoServico.listarNome(textoPesquisaFormaPagamento);
		}else{
			formasPagamentoPesquisa = formaPagamentoServico.listarTodos();			
		}
		return formasPagamentoPesquisa;
	}
	
	public void excluir(FormaPagamento formaPagmento){
		this.formaPagamentoServico.excluir(formaPagmento);
	}
	

}
