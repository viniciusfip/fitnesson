package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.ContaAReceber;

@Transactional
public interface ContaAReceberRepositorio extends JpaRepository<ContaAReceber, Long>,
		JpaSpecificationExecutor<ContaAReceber> {
	
	@Query("SELECT conta FROM ContaAReceber conta where conta.situacaoConta = 0 AND conta.cliente.id = :idCliente")
	public List<ContaAReceber> listaContasAbertas(@Param("idCliente") Long idCliente);

}
