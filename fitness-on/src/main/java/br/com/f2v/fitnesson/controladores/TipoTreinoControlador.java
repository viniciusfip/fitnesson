package br.com.f2v.fitnesson.controladores;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.TipoTreino;
import br.com.f2v.fitnesson.servicos.TipoTreinoServico;


@ManagedBean
@ViewScoped
@Component
public class TipoTreinoControlador {
	
	@Inject
	private TipoTreino tipoTreino;
	
	@Inject
	private TipoTreinoServico tipoTreinoServico;
	
	private String textoPesquisaTreino;
	
	private List<TipoTreino> tiposTreino = new ArrayList<TipoTreino>();

	public TipoTreino getTipoTreino() {
		return tipoTreino;
	}

	public void setTipoTreino(TipoTreino tipoTreino) {
		this.tipoTreino = tipoTreino;
	}
		
	public String getTextoPesquisaTreino() {
		return textoPesquisaTreino;
	}

	public void setTextoPesquisaTreino(String textoPesquisaTreino) {
		this.textoPesquisaTreino = textoPesquisaTreino;
	}

	public void salvar(){
		this.tipoTreinoServico.salvarTipoTreino(tipoTreino);
		setTipoTreino(new TipoTreino());
	}

	public String chamaTelaPesquisa(){
		return "pesquisarTelaTipoTreino";
	}
	
	public String alteraTipoTreino(TipoTreino tipoTreino){
		this.tipoTreino = tipoTreino;
		return "cadastraTreino.xhtml";		
	}
	
	public List<TipoTreino> listarTreinos(){
		if (textoPesquisaTreino != null) {
			tiposTreino = tipoTreinoServico.pesquisaPeloNome(textoPesquisaTreino);			
		}else{
			tiposTreino = tipoTreinoServico.listarTodos();
		}
		
		return tiposTreino;
	}
	
	public void excluir(TipoTreino tipoTreino){
		this.tipoTreinoServico.excluir(tipoTreino);
	}
}
