package br.com.f2v.fitnesson.controladores;

import java.util.HashSet;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Cargo;
import br.com.f2v.fitnesson.entidades.Cliente;
import br.com.f2v.fitnesson.entidades.ClienteEndereco;
import br.com.f2v.fitnesson.entidades.Endereco;
import br.com.f2v.fitnesson.entidades.Funcionario;
import br.com.f2v.fitnesson.entidades.FuncionarioEndereco;
import br.com.f2v.fitnesson.enumeradores.GeneroEnum;
import br.com.f2v.fitnesson.enumeradores.StatusFuncionarioEnum;
import br.com.f2v.fitnesson.servicos.CargoServico;
import br.com.f2v.fitnesson.servicos.FuncionarioEnderecoServico;

@ManagedBean
@ViewScoped
@Component
public class FuncionarioControlador {

	@Inject
	private Funcionario funcionario;
	
	@Inject
	private FuncionarioEnderecoServico funcionarioEnderecoServico;
	
	@Inject
	private Endereco endereco;
	
	@Inject
	private FuncionarioEndereco funcionarioEndereco;
	
	@Inject
	private CargoServico cargoServico;
	
	private StatusFuncionarioEnum statusFuncionarioEnum;
	
	private GeneroEnum generoEnum;
	
	private Long cargoId;

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public FuncionarioEndereco getFuncionarioEndereco() {
		return funcionarioEndereco;
	}

	public void setFuncionarioEndereco(FuncionarioEndereco funcionarioEndereco) {
		this.funcionarioEndereco = funcionarioEndereco;
	}

	public StatusFuncionarioEnum[] getStatusFuncionarioEnum() {
		return StatusFuncionarioEnum.values();
	}

	public void setStatusFuncionarioEnum(StatusFuncionarioEnum statusFuncionarioEnum) {
		this.statusFuncionarioEnum = statusFuncionarioEnum;
	}

	public GeneroEnum[] getGeneroEnums() {
		return GeneroEnum.values();
	}

	public void setGeneroEnum(GeneroEnum generoEnum) {
		this.generoEnum = generoEnum;
	}
	
	
	public Long getCargoId() {
		return cargoId;
	}

	public void setCargoId(Long cargoId) {
		this.cargoId = cargoId;
	}

	public List<Cargo> listarTodosCargos(){
		return cargoServico.listarTodos();
	}
	
	public String salvarFuncionario(){
		funcionario.setEnderecos(new HashSet<>());
		funcionario.getEnderecos().add(funcionarioEndereco);

		funcionarioEndereco.setFuncionario(funcionario);
		funcionarioEnderecoServico.salvar(funcionarioEndereco);
	
		novoFuncionario();

		return "funcionarioCadastrado";
	}
	
	public void novoFuncionario(){
		setFuncionario(new Funcionario());
		setEndereco(new Endereco());
	    setFuncionarioEndereco(new FuncionarioEndereco());
	}
	
	
	
}
