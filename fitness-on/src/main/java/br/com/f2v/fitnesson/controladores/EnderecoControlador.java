package br.com.f2v.fitnesson.controladores;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Endereco;
import br.com.f2v.fitnesson.servicos.EnderecoServico;

@ManagedBean
@ViewScoped
@Component
public class EnderecoControlador {

	@Inject
	private EnderecoServico enderecoServico;
	
	@Inject
	private Endereco endereco;

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public void salvarEndereco(){
		enderecoServico.salvar(endereco);
	}
}
