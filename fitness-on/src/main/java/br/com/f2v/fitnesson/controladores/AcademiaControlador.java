package br.com.f2v.fitnesson.controladores;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Academia;
import br.com.f2v.fitnesson.servicos.AcademiaServico;

@ManagedBean
@ViewScoped
@Component
public class AcademiaControlador {

	@Inject
	private AcademiaServico academiaServico;
	
	@Inject 
	private Academia academia;
	
	@PostConstruct
	public void init() {
		academia = academiaServico.listarAcademia();
	}
	
	public Academia getAcademia() {
		return academia;
	}

	public void setAcademia(Academia academia) {
		this.academia = academia;
	}
	
	public void salvar(){
		academiaServico.salvar(academia);
		setAcademia(new Academia());
	}
	
}
