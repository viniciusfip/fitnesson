package br.com.f2v.fitnesson.entidades;


import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.transientes.TelefoneInterface;
import br.com.f2v.fitnesson.enumeradores.TelefoneTipoEnum;

/**
 * 
 * Classe que representa a entidade ClienteTelefone
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
@Entity
@Table
@Component
@Scope("prototype")
public class ClienteTelefone implements TelefoneInterface {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private TelefoneTipoEnum telefoneTipo;
	
	private String numero;
	
	@Inject
	@OneToOne(cascade = {CascadeType.DETACH, 
			CascadeType.MERGE, 
			CascadeType.REFRESH, 
			CascadeType.PERSIST})
	private Cliente cliente;

	@Override
	public Long getId() {
		return this.id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public TelefoneTipoEnum getTelefoneTipo() {
		return this.telefoneTipo;
	}
	@Override
	public void setTelefoneTipo(TelefoneTipoEnum telefoneTipo) {
		this.telefoneTipo = telefoneTipo;
	}

	@Override
	public String getNumero() {
		return this.numero;
	}
	@Override
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public Cliente getCliente() {
		return this.cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}