package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.Cliente;
import br.com.f2v.fitnesson.entidades.Fornecedor;


@Transactional
public interface FornecedorRepositorio extends JpaRepository<Fornecedor, Long>, JpaSpecificationExecutor<Fornecedor>{
	
	@Query("SELECT f FROM Fornecedor f where f.nome LIKE %:nome% ORDER BY f.nome ASC")
	public List<Fornecedor> buscaPeloNome(@Param("nome") String nome);
	
	@Query("SELECT f FROM Fornecedor f where f.fantasia LIKE %:fantasia% ORDER BY f.fantasia ASC")
	public List<Fornecedor> buscaPelaFantasia(@Param("fantasia") String fantasia);
	
	@Query("SELECT f FROM Fornecedor f where f.cnpjCpf = :cnpjCpf ORDER BY f.nome ASC")
	public List<Fornecedor> buscaPeloCnpjCpf(@Param("cnpjCpf") String cnpjCpf);
	
	
	
	

}
