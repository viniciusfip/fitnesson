package br.com.f2v.fitnesson.controladores;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.Academia;
import br.com.f2v.fitnesson.entidades.Cliente;
import br.com.f2v.fitnesson.entidades.ContaAReceber;
import br.com.f2v.fitnesson.entidades.Exercicio;
import br.com.f2v.fitnesson.entidades.ExerciciosDaFicha;
import br.com.f2v.fitnesson.entidades.FichaTreino;
import br.com.f2v.fitnesson.entidades.Perimetria;
import br.com.f2v.fitnesson.entidades.PerimetriaDaFicha;
import br.com.f2v.fitnesson.entidades.TipoTreino;
import br.com.f2v.fitnesson.enumeradores.FichaTreinoEnum;
import br.com.f2v.fitnesson.servicos.AcademiaServico;
import br.com.f2v.fitnesson.servicos.ClienteServico;
import br.com.f2v.fitnesson.servicos.ContaAReceberServico;
import br.com.f2v.fitnesson.servicos.ExercicioServico;
import br.com.f2v.fitnesson.servicos.ExerciciosDaFichaServico;
import br.com.f2v.fitnesson.servicos.FichaTreinoServico;
import br.com.f2v.fitnesson.servicos.PerimetriaDaFichaServico;
import br.com.f2v.fitnesson.servicos.PerimetriaServico;
import br.com.f2v.fitnesson.servicos.TipoTreinoServico;




@ManagedBean
@ViewScoped
@Component
public class FichaTreinoControlador {
	
	@PersistenceUnit(name = "academia")
	private EntityManagerFactory factory;

	@Inject
	private FichaTreino fichaTreino;

	@Inject
	private Academia academia;

	@Inject
	private FichaTreinoServico fichaTreinoServico;

	@Inject
	private ExerciciosDaFicha exercicioDaFicha;

	@Inject
	private ContaAReceber contaAReceber;

	@Inject
	private ContaAReceberServico contaAReceberServico;

	@Inject
	private ExerciciosDaFichaServico exerciciosDaFichaServico;

	@Inject
	private Cliente cliente;

	@Inject
	private Exercicio exercicio;

	@Inject
	private TipoTreino tipoTreino;

	@Inject
	private PerimetriaDaFichaServico perimetriaDaFichaServico;

	@Inject
	private PerimetriaDaFicha perimetriaDaFicha;

	@Inject
	private PerimetriaServico perimetriaServico;

	@Inject
	private ExercicioServico exercicioServico;

	@Inject
	private TipoTreinoServico tipoTreinoServico;

	@Inject
	private ClienteServico clienteServico;

	@Inject
	private AcademiaServico academiaServico;

	private long clienteId;

	private long exercicioId;

	private long tipoTreinoId;

	private long perimetriaId;

	private Integer activeIndex = 0;

	private String opcaoSelectMenu;

	private String textoPesquisa;

	private String txtImprimeSerie;

	private SelectOneMenu selectMenu;
	
	private Date inicio, fim;

	private List<ExerciciosDaFicha> montarSerie = new ArrayList<ExerciciosDaFicha>();
	private List<ExerciciosDaFicha> exerciciosFichaAndamento = new ArrayList<ExerciciosDaFicha>();

	private List<PerimetriaDaFicha> perimetrias = new ArrayList<PerimetriaDaFicha>();
	private List<PerimetriaDaFicha> exibePerimetrias = new ArrayList<PerimetriaDaFicha>();
	private List<ExerciciosDaFicha> exibeExercicios = new ArrayList<ExerciciosDaFicha>();
	private List<ContaAReceber> contasAbertas = new ArrayList<ContaAReceber>();
	private List<FichaTreino> fichas = new ArrayList<FichaTreino>();
	private List<Cliente> clientes = new ArrayList<Cliente>();

	Date hoje = new Date();

	private FichaTreinoEnum fichaTreinoEnum;

	private DataTable dataTable = new DataTable();

	@PostConstruct
	public void init() {
		Date hoje = new Date();
		fichaTreino.setDataInicio(hoje);

		opcaoSelectMenu = "Sequencia";

	}

	public SelectOneMenu getSelectMenu() {
		return selectMenu;
	}

	public void setSelectMenu(SelectOneMenu selectMenu) {
		this.selectMenu = selectMenu;
	}

	public FichaTreino getFichaTreino() {
		return fichaTreino;
	}

	public void setFichaTreino(FichaTreino fichaTreino) {
		this.fichaTreino = fichaTreino;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public Academia getAcademia() {
		return academia;
	}

	public void setAcademia(Academia academia) {
		this.academia = academia;
	}

	public ContaAReceber getContaAReceber() {
		return contaAReceber;
	}

	public void setContaAReceber(ContaAReceber contaAReceber) {
		this.contaAReceber = contaAReceber;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}

	public long getClienteId() {
		return clienteId;
	}

	public String getTxtImprimeSerie() {
		return txtImprimeSerie;
	}

	public void setTxtImprimeSerie(String txtImprimeSerie) {
		this.txtImprimeSerie = txtImprimeSerie;
	}

	public long getExercicioId() {
		return exercicioId;
	}

	public void setExercicioId(long exercicioId) {
		this.exercicioId = exercicioId;
	}

	public long getTipoTreinoId() {
		return tipoTreinoId;
	}

	public void setTipoFichaId(long tipoTreinoId) {
		this.tipoTreinoId = tipoTreinoId;
	}

	public void setClienteId(long clienteId) {
		this.clienteId = clienteId;
	}

	public ExerciciosDaFicha getExercicioDaFicha() {
		return exercicioDaFicha;
	}

	public void setExercicioDaFicha(ExerciciosDaFicha exercicioDaFicha) {
		this.exercicioDaFicha = exercicioDaFicha;
	}

	public Exercicio getExercicio() {
		return exercicio;
	}

	public void setExercicio(Exercicio exercicio) {
		this.exercicio = exercicio;
	}

	public TipoTreino getTipoTreino() {
		return tipoTreino;
	}

	public void setTipoTreino(TipoTreino tipoTreino) {
		this.tipoTreino = tipoTreino;
	}

	public void setTipoTreinoId(long tipoTreinoId) {
		this.tipoTreinoId = tipoTreinoId;
	}

	public long getPerimetriaId() {
		return perimetriaId;
	}

	public void setPerimetriaId(long perimetriaId) {
		this.perimetriaId = perimetriaId;
	}

	public PerimetriaDaFicha getPerimetriaDaFicha() {
		return perimetriaDaFicha;
	}

	public void setPerimetriaDaFicha(PerimetriaDaFicha perimetriaDaFicha) {
		this.perimetriaDaFicha = perimetriaDaFicha;
	}

	public List<Perimetria> listarPerimetrias() {
		return perimetriaServico.listarTodos();
	}

	public FichaTreinoEnum[] getFichaTreinoEnum() {
		return FichaTreinoEnum.values();
	}

	public void setFichaTreinoEnum(FichaTreinoEnum fichaTreinoEnum) {
		this.fichaTreinoEnum = fichaTreinoEnum;
	}
	
	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

	public List<Cliente> listarTodosClientes() {

		if (clientes.isEmpty()) {
			clientes = clienteServico.listaTodosAlunos();
		} else {
			// this.setClienteId(fichaTreino.getCliente().getId());
			// clientes =
			// clienteServico.recuperaListaPeloId(fichaTreino.getCliente().getId());
		}
		return clientes;
	}

	public List<Exercicio> listarTodosExercicios() {
		return exercicioServico.listarTodosExercicios();
	}

	public List<ExerciciosDaFicha> listarExerciciosDaFicha() {
		if (fichaTreino.getId() != null) {
			exibeExercicios = exerciciosDaFichaServico.exerciciosDaFicha(
					fichaTreino.getId(), fichaTreino.getCliente().getId());
		}
		return exibeExercicios;
	}

	// validar esse m�todo. digitar o cpf nao cadastrado dar erro. Fazer testes
	// nesse metodo
	public List<ExerciciosDaFicha> listaExercicioDiario() {
		LocalDate hoje = LocalDate.now();
		System.out.println("dia da semana " + hoje.getDayOfWeek());
		System.out.println("select menu " + opcaoSelectMenu);

		if (txtImprimeSerie != null) {
			fichaTreino = fichaTreinoServico.pesquisaFichaAndamento(txtImprimeSerie);

			if (opcaoSelectMenu.equalsIgnoreCase("Sequencia")) {
				if (fichaTreino.getSequenciaTreino() == 1) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getSegunda());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				} else if (fichaTreino.getSequenciaTreino() == 2) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getTerca());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				} else if (fichaTreino.getSequenciaTreino() == 3) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getQuarta());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				} else if (fichaTreino.getSequenciaTreino() == 4) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getQuinta());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				} else if (fichaTreino.getSequenciaTreino() == 5) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getSexta());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				} else if (fichaTreino.getSequenciaTreino() == 6) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getSabado());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				}

			}
			
			if (opcaoSelectMenu.equalsIgnoreCase("Dia")) {
				if (hoje.getDayOfWeek().name().equalsIgnoreCase("Monday")) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getSegunda());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());

				} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Tuesday")) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getTerca());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());

				} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Wednesday")) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getQuarta());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());

				} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Thursday")) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getQuinta());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Friday")) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getSexta());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());

				} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Saturday")) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getSabado());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Sunday")) {
					tipoTreino = tipoTreinoServico.buscaPeloNome(fichaTreino.getDomingo());
					exerciciosFichaAndamento = exerciciosDaFichaServico.exerciciosFichaAndamento(fichaTreino.getId(),tipoTreino.getId());
				}
			}				
		}

		return exerciciosFichaAndamento;
	}
	
	/*
	 * Quando imprimir devo limpar o grid e input de pesquisa.
	 * 
	 */

	public void imprimeExerciciosDiarios() throws JRException {
		
		academia = academiaServico.listarAcademia();
		HashMap<String, Object> mapAcademia = new HashMap<String, Object>();
		mapAcademia.put("nomeAcademia", academia.getNome());
		mapAcademia.put("rua", academia.getEndereco());
		mapAcademia.put("bairro", academia.getBairro());
		mapAcademia.put("cidade", academia.getMunicipio());
		mapAcademia.put("telefone", academia.getTelefone1());
		
		/*
		 * Compara se o aluno treina aos Sabados e caso nao treine a sequencia � inicializada.		  
		 */						
		if(fichaTreino.getSabado() == null || fichaTreino.getSabado().isEmpty()){			
			if (fichaTreino.getSequenciaTreino()==5) {				
				fichaTreino.setSequenciaTreino(0);
			}			
		}
		
		/*
		 * O aluno treina no Sabado e sequencia � inicializada.
		 */
		if (fichaTreino.getSequenciaTreino()>=6) {			
			fichaTreino.setSequenciaTreino(0);
		}
		
		
		fichaTreino.setSequenciaTreino(fichaTreino.getSequenciaTreino() +1);
		fichaTreinoServico.salvarFichaTreino(fichaTreino);
		
		

		JasperReport report = JasperCompileManager
				.compileReport("D:/Projetos/fitness-on/src/main/webapp/relatorios/exerciciosDiarios.jrxml");
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
				exerciciosFichaAndamento, false);
		JasperPrint print = JasperFillManager.fillReport(report, mapAcademia,
				dataSource);
		JasperViewer jv = new JasperViewer(print, false);
		jv.setVisible(true);
	}

	public List<ContaAReceber> listarContasAbertas() {
		if (fichaTreino.getId() != null) {
			contasAbertas = contaAReceberServico.listaContasAbertas(fichaTreino
					.getCliente().getId());
		}
		return contasAbertas;
	}

	public List<ExerciciosDaFicha> listarTodosExercicioFicha() {
		return exerciciosDaFichaServico.listarTodosExercicioFicha();
	}

	public List<PerimetriaDaFicha> listarPerimetriaDaFicha() {
		if (fichaTreino.getId() != null) {
			exibePerimetrias = perimetriaDaFichaServico
					.listarPerimetriaDaFicha(fichaTreino.getId(), fichaTreino
							.getCliente().getId());
		}
		return exibePerimetrias;
	}

	public List<TipoTreino> listarTodosTiposTreinos() {
		return tipoTreinoServico.listarTodos();
	}

	public List<ExerciciosDaFicha> exerciciosDaLista() {
		return montarSerie;
	}

	public List<PerimetriaDaFicha> perimetriaDaLista() {
		return perimetrias;
	}

	public void adicionaExercicioNoList() {
		Cliente clienteSelecionado = clienteServico
				.recuperaClienteID(clienteId);
		TipoTreino tipoSelecionado = tipoTreinoServico
				.buscarPorId(tipoTreinoId);
		Exercicio exercicioSelecionado = exercicioServico
				.buscaPorId(exercicioId);
		exercicioDaFicha.setCliente(clienteSelecionado);
		exercicioDaFicha.setTipoTreino(tipoSelecionado);
		exercicioDaFicha.setExercicio(exercicioSelecionado);

		montarSerie.add(exercicioDaFicha);
		exercicioDaFicha = new ExerciciosDaFicha();
	}

	public void adicionaPerimetriaNoList() {
		Cliente clienteSelecionado = clienteServico
				.recuperaClienteID(clienteId);
		perimetriaDaFicha.setDataInicialPerimetria(hoje);
		perimetriaDaFicha.setCliente(clienteSelecionado);
		perimetrias.add(perimetriaDaFicha);
		perimetriaDaFicha = new PerimetriaDaFicha();
	}

	public void salvar() {
		FacesContext context = FacesContext.getCurrentInstance();

		fichaTreino.setExerciciosDaFicha(new HashSet<>());
		fichaTreino.setPerimetriaDaFicha(new HashSet<>());

		Cliente clienteSelecionado = clienteServico
				.recuperaClienteID(clienteId);
		fichaTreino.setCliente(clienteSelecionado);

		for (ExerciciosDaFicha e : montarSerie) {
			exercicioDaFicha.setPosicao(e.getPosicao());
			exercicioDaFicha.setSerie(e.getSerie());
			exercicioDaFicha.setRepeticoes(e.getRepeticoes());
			exercicioDaFicha.setCarga(e.getCarga());
			exercicioDaFicha.setCliente(e.getCliente());
			exercicioDaFicha.setTipoTreino(e.getTipoTreino());
			exercicioDaFicha.setExercicio(e.getExercicio());
			exercicioDaFicha.setFichaTreino(fichaTreino);

			fichaTreino.getExerciciosDaFicha().add(exercicioDaFicha);
			exercicioDaFicha = new ExerciciosDaFicha();
		}

		for (PerimetriaDaFicha p : perimetrias) {
			perimetriaDaFicha.setDataInicialPerimetria(hoje);
			perimetriaDaFicha.setAbdomem(p.getAbdomem());
			perimetriaDaFicha.setAltura(p.getAltura());
			perimetriaDaFicha.setAnteBracoDir(p.getAnteBracoDir());
			perimetriaDaFicha.setAnteBracoEsq(p.getAnteBracoEsq());
			perimetriaDaFicha.setBracoDir(p.getBracoDir());
			perimetriaDaFicha.setBracoEsq(p.getBracoEsq());
			perimetriaDaFicha.setCintura(p.getCintura());
			perimetriaDaFicha.setCoxaDir(p.getCoxaDir());
			perimetriaDaFicha.setCoxaEsq(p.getCoxaEsq());
			perimetriaDaFicha.setOmbro(p.getOmbro());
			perimetriaDaFicha.setPanturilhaDir(p.getPanturilhaDir());
			perimetriaDaFicha.setPanturilhaEsq(p.getPanturilhaEsq());
			perimetriaDaFicha.setPeso(p.getPeso());
			perimetriaDaFicha.setQuadril(p.getQuadril());
			perimetriaDaFicha.setTorax(p.getTorax());
			perimetriaDaFicha.setCliente(p.getCliente());
			perimetriaDaFicha.setFichaTreino(fichaTreino);

			fichaTreino.getPerimetriaDaFicha().add(perimetriaDaFicha);
			perimetriaDaFicha = new PerimetriaDaFicha();

		}
		fichaTreino.setFichaEnum(FichaTreinoEnum.Andamento);

		LocalDate hoje = LocalDate.now();

		if (hoje.getDayOfWeek().name().equalsIgnoreCase("Monday")) {
			fichaTreino.setSequenciaTreino(1);

		} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Tuesday")) {
			fichaTreino.setSequenciaTreino(2);

		} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Wednesday")) {
			fichaTreino.setSequenciaTreino(3);

		} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Thursday")) {
			fichaTreino.setSequenciaTreino(4);
		} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Friday")) {
			fichaTreino.setSequenciaTreino(5);

		} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Saturday")) {
			fichaTreino.setSequenciaTreino(6);
		} else if (hoje.getDayOfWeek().name().equalsIgnoreCase("Sunday")) {
			fichaTreino.setSequenciaTreino(1);
		}

		fichaTreinoServico.salvarFichaTreino(fichaTreino);
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Aluno Cadastrado com sucesso!",
						"Aluno Cadastrado com sucesso!"));

		// fichaTreino = new FichaTreino();
	}

	public void excluirPerimetriaList(PerimetriaDaFicha perimetria) {
		perimetrias.remove(perimetria);
	}

	public void excluirExercicioList(ExerciciosDaFicha exercicio) {
		montarSerie.remove(exercicio);
	}

	public List<FichaTreino> pesquisarFichas() {
		if (opcaoSelectMenu.equals("Nome")) {
			fichas = fichaTreinoServico.pesquisaAlunoNome(textoPesquisa);
		} else if (opcaoSelectMenu.equals("Matricula")) {
			fichas = fichaTreinoServico.pesquisaAlunoMatricula(textoPesquisa);
		} else if (opcaoSelectMenu.equalsIgnoreCase("CPF")) {
			fichas = fichaTreinoServico.pesquisaAlunoCpf(textoPesquisa);
		} else {
			fichas = fichaTreinoServico.listarTodas();
		}
		return fichas;
	}

	public String alterarFicha(FichaTreino fichaTreino) {

		this.fichaTreino = fichaTreino;

		this.setClienteId(fichaTreino.getCliente().getId());
		return "fichaAluno.xhtml";

	}

	public String getTextoPesquisa() {
		return textoPesquisa;
	}

	public void setTextoPesquisa(String textoPesquisa) {
		this.textoPesquisa = textoPesquisa;
	}

	public String getOpcaoSelectMenu() {
		return opcaoSelectMenu;
	}

	public void setOpcaoSelectMenu(String opcaoSelectMenu) {
		this.opcaoSelectMenu = opcaoSelectMenu;
	}

	public void chamaTelaPesquisaTreino() {
		
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("modal", true);
		opcoes.put("resizable", false);
		opcoes.put("contentHeight", 470);
		opcoes.put("contentWidth", 820);
		
		RequestContext.getCurrentInstance().openDialog("/paginas/treino/pesquisaFicha", opcoes, null);
		
	}
	
	public void selecionarFicha(FichaTreino ficha){
		RequestContext.getCurrentInstance().closeDialog(ficha);
	}
	
	public void fichaSelecionado(SelectEvent event){
		FichaTreino ficha = (FichaTreino) event.getObject();
		this.setFichaTreino(ficha);
	}
	
	public List<FichaTreino> listaPorFiltros(){
		EntityManager entityManager = factory.createEntityManager();
		System.out.println("USANDO CRITERIA.....");
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<FichaTreino> criteria = builder.createQuery(FichaTreino.class);
		Root<FichaTreino> ficha = criteria.from(FichaTreino.class);
		criteria.select(ficha);
		
		List<Predicate> predicados = new ArrayList<Predicate>();
		
		if (this.textoPesquisa != null) {	
				predicados.add(builder.like(ficha.get("nome"), textoPesquisa));
		}
		
		if (predicados.size() > 0) {
			System.out.println("predicados SIZE");
			criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
		}
		
		TypedQuery<FichaTreino> query = entityManager.createQuery(criteria);
		List<FichaTreino> fichas = query.getResultList();
		
		//fichasTmp = fichaServicoConsulta.listarTodos();
		
		return fichas;
	}
	
	

	/*
	 * public void onTabChange(TabChangeEvent event) { // FacesMessage msg = new
	 * FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());
	 * 
	 * //FichaTreino fichaTreinoSelecionada = fichaTreinoServico.buscarPorId(f)
	 * 
	 * TabView tabView = (TabView) event.getComponent(); int activeTabIndex =
	 * tabView.getActiveIndex(); System.out.println("--------" +
	 * activeTabIndex);
	 * 
	 * System.out.println("NUMERO DA FICHA " +fichaTreino.getId());
	 * System.out.println("NUMERO DA cliente "
	 * +fichaTreino.getCliente().getId());
	 * 
	 * 
	 * if (activeTabIndex == 3 ) { System.out.println("estou aqui 777777777");
	 * listarExerciciosDaFicha();
	 * 
	 * }
	 * 
	 * 
	 * 
	 * // FacesContext context = FacesContext.getCurrentInstance(); //
	 * Map<String, String> params =
	 * context.getExternalContext().getRequestParameterMap(); //String
	 * activeIndexValue = params.get(tabView.getClientId(context) +
	 * "_activeIndex"); //System.out.println("--------" + activeIndexValue);
	 * 
	 * //context.addMessage(null, msg); }
	 */

}
