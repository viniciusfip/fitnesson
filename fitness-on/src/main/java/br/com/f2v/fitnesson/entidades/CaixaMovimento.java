package br.com.f2v.fitnesson.entidades;

import java.time.LocalDate;
import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;



@Component
@Scope("prototype")
@Entity
public class CaixaMovimento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id;
	
	/*@Inject
	@OneToOne	
	private ContaAReceber contaAReceber;
		
	@Inject
	@OneToOne
	private ContaAPagar contaAPagar;*/
	
	@Inject
	@OneToOne
	private FormaPagamento formaPagamento;
	
	@Transient
	private Date dataInicial;
	
	@Transient
	private Date dataFinal;
	
	@Transient
	private double valorLancamento;
	
	private double valorCredito;	
	
	private double valorDebito;
		
	private Date dataTransacao;		
	
		
	private String obs;
	
	private Long recebeIdConta;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public double getValorCredito() {
		return valorCredito;
	}

	public void setValorCredito(double valorCredito) {
		this.valorCredito = valorCredito;
	}

	public double getValorDebito() {
		return valorDebito;
	}

	public void setValorDebito(double valorDebito) {
		this.valorDebito = valorDebito;
	}

	public Date getDataTransacao() {
		return dataTransacao;
	}

	public void setDataTransacao(Date dataTransacao) {
		this.dataTransacao = dataTransacao;
	}
	
	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public Long getRecebeIdConta() {
		return recebeIdConta;
	}

	public void setRecebeIdConta(Long recebeIdConta) {
		this.recebeIdConta = recebeIdConta;
	}

	public double getValorLancamento() {
		return valorLancamento;
	}

	public void setValorLancamento(double valorLancamento) {
		this.valorLancamento = valorLancamento;
	}

	
	
	
	

	


}

