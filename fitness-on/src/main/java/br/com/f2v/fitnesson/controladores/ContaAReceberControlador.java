package br.com.f2v.fitnesson.controladores;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.CaixaMovimento;
import br.com.f2v.fitnesson.entidades.Cliente;
import br.com.f2v.fitnesson.entidades.ContaAReceber;
import br.com.f2v.fitnesson.entidades.FormaPagamento;
import br.com.f2v.fitnesson.enumeradores.EntraNoCaixaEnum;
import br.com.f2v.fitnesson.enumeradores.SituacaoContaAReceberEnum;
import br.com.f2v.fitnesson.servicos.CaixaMovimentoServico;
import br.com.f2v.fitnesson.servicos.ClienteServico;
import br.com.f2v.fitnesson.servicos.ContaAReceberServico;
import br.com.f2v.fitnesson.servicos.FormaPagamentoServico;

@ManagedBean
@ViewScoped
@Component
public class ContaAReceberControlador {

	@Inject
	private ContaAReceber contaAReceber;

	@Inject
	private ContaAReceberServico contaAReceberServico;

	@Inject
	private FormaPagamento formaPagamento;

	@Inject
	private FormaPagamentoServico formaPagamentoServico;

	@Inject
	private Cliente cliente;

	@Inject
	private ClienteServico clienteServico;

	@Inject
	private CaixaMovimentoServico caixaMovimentoServico;
	
	@Inject
	private CaixaMovimento caixaMovimento;

	private SituacaoContaAReceberEnum situcaoContaAReceberEnum;

	private long clienteId;

	private long formaPagamentoId;

	public ContaAReceber getContaAReceber() {
		return contaAReceber;
	}

	public void setContaAReceber(ContaAReceber contaAReceber) {
		this.contaAReceber = contaAReceber;
	}

	public ContaAReceberServico getContaAReceberServico() {
		return contaAReceberServico;
	}

	public void setContaAReceberServico(
			ContaAReceberServico contaAReceberServico) {
		this.contaAReceberServico = contaAReceberServico;
	}

	public SituacaoContaAReceberEnum[] getSituacaoContaAReceberEnums() {
		return SituacaoContaAReceberEnum.values();
	}

	public void setSituacaoContaAReceberEnum(
			SituacaoContaAReceberEnum situacaoContaAReceberEnum) {
		this.situcaoContaAReceberEnum = situacaoContaAReceberEnum;

	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public ClienteServico getClienteServico() {
		return clienteServico;
	}

	public void setClienteServico(ClienteServico clienteServico) {
		this.clienteServico = clienteServico;
	}

	public long getClienteId() {
		return clienteId;
	}

	public void setClienteId(long clienteId) {
		this.clienteId = clienteId;
	}

	public long getFormaPagamentoId() {
		return formaPagamentoId;
	}

	public void setFormaPagamentoId(long formaPagamentoId) {
		this.formaPagamentoId = formaPagamentoId;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public FormaPagamentoServico getFormaPagamentoServico() {
		return formaPagamentoServico;
	}

	public void setFormaPagamentoServico(
			FormaPagamentoServico formaPagamentoServico) {
		this.formaPagamentoServico = formaPagamentoServico;
	}

	public List<Cliente> listarTodosClientes() {
		return clienteServico.listaTodosAlunos();
	}

	public List<FormaPagamento> listarTodasFormasPagamento() {
		return formaPagamentoServico.listarTodos();
	}

	public void salvar() {
		
		Cliente clienteSelecionado = clienteServico
				.recuperaClienteID(clienteId);
		contaAReceber.setCliente(clienteSelecionado);
		FormaPagamento formaSelecionada = formaPagamentoServico
				.buscarPorId(formaPagamentoId);
		contaAReceber.setFormaPagamentoParcela(formaSelecionada);
		contaAReceber.setSituacaoConta(situcaoContaAReceberEnum.ABERTA);

		contaAReceberServico.salvar(contaAReceber);		
		//novaConta();
		
	}

	public void novaConta() {
		setCliente(new Cliente());
		//setContaAReceber(new ContaAReceber());
		setFormaPagamento(new FormaPagamento());

	}

	public void calculaValorAReceberComDesconto() {
		double resultado;
		if (this.contaAReceber.getDesconto() > 0) {
			contaAReceber.setJuros(0.0);
			resultado = contaAReceber.getValorTotal()
					- contaAReceber.getDesconto();
			contaAReceber.setValorReceber(resultado);
		}
	}

	public void calculaValorAReceberComJuros() {
		double resultado;
		if (this.contaAReceber.getJuros() > 0) {
			contaAReceber.setDesconto(0.0);
			resultado = contaAReceber.getValorTotal()
					+ contaAReceber.getJuros();
			contaAReceber.setValorReceber(resultado);
		}
	}

	public void calculaValorRecebercomValorTotal() {		
		contaAReceber.setDesconto(0.0);
		contaAReceber.setJuros(0.0);
		contaAReceber.setValorReceber(contaAReceber.getValorTotal());
	}

	public void liquidaConta() {
		System.out.println("valor recebido "+contaAReceber.getValorRecebido());
		
		//contaAReceber = contaAReceberServico.recuperaContaID(contaAReceber.getId());
		
		contaAReceber.setSituacaoConta(SituacaoContaAReceberEnum.RECEBIDA);
		
		
	    contaAReceberServico.salvar(contaAReceber);
		FormaPagamento formaSelecionada = formaPagamentoServico.buscarPorId(formaPagamentoId);

		// Se no cadastra forma de pagamento o entra no caixa estiver como
		// SIM. Insere no caixa diario

		if (formaSelecionada.getEntraNoCaixa() == EntraNoCaixaEnum.SIM) {
			CaixaMovimento caixaMovimento = new CaixaMovimento();

			Date hoje = new Date();
			caixaMovimento.setDataTransacao(hoje);

			caixaMovimento.setValorCredito(contaAReceber.getValorRecebido());			

			caixaMovimento.setRecebeIdConta(contaAReceber.getId());
			caixaMovimento.setFormaPagamento(formaSelecionada);
			
			System.out.println("valor recebido "+contaAReceber.getValorRecebido()+ " "+ "forma pagamento "+formaSelecionada.getId());
			
			caixaMovimentoServico.salvar(caixaMovimento);
		}

	}

}
