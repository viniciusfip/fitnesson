package br.com.f2v.fitnesson.controladores;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIForm;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.springframework.stereotype.Component;
import org.springframework.web.jsf.FacesContextUtils;

import br.com.f2v.fitnesson.entidades.Cliente;
import br.com.f2v.fitnesson.entidades.ClienteEndereco;
import br.com.f2v.fitnesson.entidades.Endereco;
import br.com.f2v.fitnesson.enumeradores.GeneroEnum;
import br.com.f2v.fitnesson.enumeradores.StatusClienteEnum;
import br.com.f2v.fitnesson.servicos.ClienteEnderecoServico;
import br.com.f2v.fitnesson.servicos.ClienteServico;
import br.com.f2v.fitnesson.servicos.EnderecoServico;

@ManagedBean
@ViewScoped
@Component
public class ClienteControlador {
	
	@Inject
	private ClienteServico clienteServico;
	
	@Inject
	private EnderecoServico enderecoServico;
	
	@Inject
	private ClienteEnderecoServico clienteEnderecoServico;
	
	@Inject
	private Cliente cliente;
	
	@Inject
	private Endereco endereco;
	
	@Inject
	private ClienteEndereco clienteEndereco;
	
	private StatusClienteEnum statusClienteEnum; 
	
	private GeneroEnum generoEnum;
	
	private Date dataAtual;
	
	private List<Cliente> listaAlunos = new ArrayList<Cliente>();	
	
	private List<ClienteEndereco> listaAlunosEnderecos = new ArrayList<ClienteEndereco>();
	
	private String textoPesquisa;
	
	private String opcaoSelectMenu;
	
	private SelectOneMenu selectMenu;
	
	
	@PostConstruct
	public void init() {		
		opcaoSelectMenu = "opcao";		
	}

	public Cliente getCliente() {
		
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public ClienteEndereco getClienteEndereco() {
		if(clienteEndereco == null){
			clienteEndereco = new ClienteEndereco();
		}
		return clienteEndereco;
	}

	public void setClienteEndereco(ClienteEndereco clienteEndereco) {
		this.clienteEndereco = clienteEndereco;
	}
	
	
	public List<Cliente> pesquisarAlunos() {		
		//if (this.listaAlunosEnderecos.size()==0) {
			if (opcaoSelectMenu.equals("Nome")) {
				listaAlunos = clienteServico.buscaAlunos(textoPesquisa);
				//listaAlunosEnderecos = clienteEnderecoServico.pesquisaAlunoNome(textoPesquisa);
			} else if (opcaoSelectMenu.equals("Matricula")) {
				listaAlunos = clienteServico.buscaAlunosMatricula(textoPesquisa);
				//listaAlunosEnderecos =  clienteEnderecoServico.pesquisaAlunoMatricula(textoPesquisa);
			} else if(opcaoSelectMenu.equalsIgnoreCase("CPF")){
				//listaAlunosEnderecos = clienteEnderecoServico.pesquisaAlunoCpf(textoPesquisa);
				listaAlunos = clienteServico.buscaAlunoCpf(textoPesquisa);
			}else {
				listaAlunos = clienteServico.listaTodosAlunos();
			}
		//}
		
		
		return listaAlunos;
		
	}
	
	public String alterarCliente(Cliente cliente){				
		this.cliente = cliente;		
		return "CadastroAluno.xhtml";
	}

	public String salvarAluno(){	
		
			cliente.setEnderecos(new HashSet<>());
			cliente.getEnderecos().add(clienteEndereco);

			clienteEndereco.setCliente(cliente);
			clienteEnderecoServico.salvar(clienteEndereco);
			
			novoAluno();
			return "alunoCadastrado";
	}
	
	public void validarUsuario(ActionEvent event){
		
		cliente = clienteServico.pesquisaPeloCpf(cliente.getCpf());
		
		if (this.cliente == null || this.cliente.getId() != null) {
			
		}else{
			 FacesContext.getCurrentInstance().addMessage(
                     null,
                     new FacesMessage(FacesMessage.SEVERITY_WARN, "Aluno j� cadastrado. Pesquise pelo CPF!",
                                 ""));
		}
		
	}	
	
	public void limpar(ActionEvent event) {
	    UIForm form = (UIForm) event.getComponent().getParent();
	//    facesUtils.cleanSubmittedValues(form);
	}
		
	public void excluir(Cliente cliente){		
			clienteServico.excluir(cliente);		
	}
	
	public String chamaTelaPesquisaAluno(){	
		System.out.println("chamando Tela");
		return "listarAluno";
	}
	
	public Date getDataAtual() {
		return dataAtual;
	}

	public void setDataAtual(Date dataAtual) {
		this.dataAtual = dataAtual;
	}
	
	
	public String getTextoPesquisa() {
		return textoPesquisa;
	}

	public void setTextoPesquisa(String textoPesquisa) {
		this.textoPesquisa = textoPesquisa;
	}
	
	public String getOpcaoSelectMenu() {
		return opcaoSelectMenu;
	}

	public void setOpcaoSelectMenu(String opcaoSelectMenu) {
		this.opcaoSelectMenu = opcaoSelectMenu;
	}

	public GeneroEnum[] getGeneroEnums() {
		return GeneroEnum.values();
	}

	public void setGeneroEnum(GeneroEnum generoEnum) {
		this.generoEnum = generoEnum;
	}
	
 
	public StatusClienteEnum[] getStatusClienteEnum() {
		return StatusClienteEnum.values();
	}

	public void setStatusClienteEnum(StatusClienteEnum statusClienteEnum) {
		this.statusClienteEnum = statusClienteEnum;
	}

	public void novoAluno(){
		setCliente(new Cliente());
		setEndereco(new Endereco());
	    setClienteEndereco(new ClienteEndereco());
	}
	

}
