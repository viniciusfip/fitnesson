package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.Usuario;



@Transactional
public interface UsuarioRepositorio extends JpaRepository<Usuario, Long>,JpaSpecificationExecutor<Usuario>{
	
	@Query("SELECT u FROM Usuario u where u.login = :login AND u.senha = :senha")
	public Usuario listaUsuario(@Param("login") String login, @Param("senha") String senha);

}
