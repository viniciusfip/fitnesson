package br.com.f2v.fitnesson.servicos;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Endereco;
import br.com.f2v.fitnesson.repositorios.EnderecoRepositorio;

@Service
public class EnderecoServico {
	
	@Inject
	private EnderecoRepositorio enderecoRepositorio;
	
	public Endereco salvar(Endereco endereco) {
		return this.enderecoRepositorio.save(endereco);
	}

}
