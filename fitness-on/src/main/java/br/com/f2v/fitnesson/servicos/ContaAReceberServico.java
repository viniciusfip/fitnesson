package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Cliente;
import br.com.f2v.fitnesson.entidades.ContaAReceber;
import br.com.f2v.fitnesson.repositorios.ContaAReceberRepositorio;


@Service
public class ContaAReceberServico {
	@Inject
	private ContaAReceberRepositorio contaAReceberRepositorio;
	
	public ContaAReceber salvar(ContaAReceber contaAReceber){
		return this.contaAReceberRepositorio.save(contaAReceber);
		
	}
	
	public List<ContaAReceber> listarContasAReceber(){
		return this.contaAReceberRepositorio.findAll();
	}
	
	public ContaAReceber recuperaContaID(Long id){
		return this.contaAReceberRepositorio.findOne(id);
	}
	
	public List<ContaAReceber> listaContasAbertas(Long id){
		return this.contaAReceberRepositorio.listaContasAbertas(id);
	}
}
