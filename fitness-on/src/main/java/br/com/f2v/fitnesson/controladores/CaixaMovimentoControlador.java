package br.com.f2v.fitnesson.controladores;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.CaixaMovimento;
import br.com.f2v.fitnesson.entidades.FormaPagamento;
import br.com.f2v.fitnesson.servicos.CaixaMovimentoServico;
import br.com.f2v.fitnesson.servicos.FormaPagamentoServico;



@ManagedBean
@ViewScoped
@Component
public class CaixaMovimentoControlador {

	@Inject 
	private CaixaMovimento caixaMovimento;
	
	@Inject 
	private CaixaMovimentoServico caixaMovimentoServico;
	
	private List<CaixaMovimento> movimentacoes = new ArrayList<CaixaMovimento>();
	
	@Inject
	private FormaPagamentoServico formaPagamentoServico;
	
	private Date inicio,fim;
	
	@Inject 
	private FormaPagamento formaPagamento;
	
	private Long formaPagamentoId;
	
	
	//private Calendar dataInicial = Calendar.getInstance();
	
	@PostConstruct
	public void init() {
		Date hoje = new Date();		
		setInicio(hoje);
		setFim(hoje);
		
	}
	
	
	
	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

	public CaixaMovimento getCaixaMovimento() {
		return caixaMovimento;
	}

	public void setCaixaMovimento(CaixaMovimento caixaMovimento) {
		this.caixaMovimento = caixaMovimento;
	}

	public CaixaMovimentoServico getCaixaMovimentoServico() {
		return caixaMovimentoServico;
	}

	public void setCaixaMovimentoServico(CaixaMovimentoServico caixaMovimentoServico) {
		this.caixaMovimentoServico = caixaMovimentoServico;
	}
	 
		
	public Long getFormaPagamentoId() {
		return formaPagamentoId;
	}

	public void setFormaPagamentoId(Long formaPagamentoId) {
		this.formaPagamentoId = formaPagamentoId;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public void salvar(){
			
	}
		
	public List<CaixaMovimento> pesquisaMovimentacao(Date data1,Date data2){
		System.out.println("PESQUISANDOOOO");
		try {
			movimentacoes = caixaMovimentoServico.listarMovimentacaoNoIntervalo(data1,data2);
			for(CaixaMovimento ca : movimentacoes){
				System.out.println("Verifica "+ca.getId()+ " "+ca.getDataTransacao());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return movimentacoes;
	}
	
	public List<FormaPagamento> listarTodasFormasPagamento(){
		return formaPagamentoServico.listarTodos();
	}
	
	public void insereCredito(){
		
		Date hoje = new Date();
		hoje.getTime();		

		SimpleDateFormat sdf =  new SimpleDateFormat("dd/MM/yyyy");
		String dataTexto = sdf.format(hoje);
				
		caixaMovimento.setDataTransacao(hoje);
		caixaMovimento.setValorCredito(caixaMovimento.getValorLancamento());
		
		FormaPagamento formaSelecionada = formaPagamentoServico.buscarPorId(formaPagamentoId);
		caixaMovimento.setFormaPagamento(formaSelecionada);
		caixaMovimentoServico.salvar(caixaMovimento);
		novaConta();
	}
	
	public void insereDebito(){
		
		Date hoje = new Date();
		DateTimeFormatter formatador =  DateTimeFormatter.ofPattern("dd/MM/yyyy");		
		
		caixaMovimento.setDataTransacao(hoje);
		
		caixaMovimento.setValorDebito(caixaMovimento.getValorLancamento());
		FormaPagamento formaSelecionada = formaPagamentoServico.buscarPorId(formaPagamentoId);
		caixaMovimento.setFormaPagamento(formaSelecionada);
		caixaMovimentoServico.salvar(caixaMovimento);
		novaConta();	
	}
	public void novaConta(){
		setCaixaMovimento(new CaixaMovimento());
		setFormaPagamento( new FormaPagamento());
	}
}
