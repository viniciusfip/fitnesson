package br.com.f2v.fitnesson.entidades;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.entidades.transientes.Bean;
import br.com.f2v.fitnesson.enumeradores.EnderecoTipoEnum;

@Entity
@Table
@Component
@Scope("prototype")
public class FuncionarioEndereco implements Bean {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private EnderecoTipoEnum tipo;
	
	private String complemento;
	
	private String numero;
	
	@Inject
	@OneToOne(cascade = {CascadeType.DETACH, 
			CascadeType.MERGE, 
			CascadeType.REFRESH,			
			CascadeType.PERSIST})
	private Funcionario funcionario;

	@Inject
	@OneToOne(cascade = {CascadeType.DETACH, 
			CascadeType.MERGE, 
			CascadeType.REFRESH,
			CascadeType.PERSIST})
	private Endereco endereco;
	
	public FuncionarioEndereco(){
		funcionario = new Funcionario();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EnderecoTipoEnum getTipo() {
		return tipo;
	}

	public void setTipo(EnderecoTipoEnum tipo) {
		this.tipo = tipo;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	

}
