package br.com.f2v.fitnesson.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.f2v.fitnesson.entidades.ExerciciosDaFicha;

@Transactional
public interface ExercicioDaFichaRepositorio extends JpaRepository<ExerciciosDaFicha, Long>,
		JpaSpecificationExecutor<ExerciciosDaFicha> {

	@Query("SELECT e FROM ExerciciosDaFicha e where e.fichaTreino.id = :idFicha and e.cliente.id = :idCliente ORDER BY e.tipoTreino.nome ASC,e.posicao ASC  ")
	public List<ExerciciosDaFicha> bucarExercicosDaFicha(@Param("idFicha") Long idFicha, @Param("idCliente") Long idCliente);
	
	@Query("SELECT e FROM ExerciciosDaFicha e where e.fichaTreino.id = :idFicha AND e.tipoTreino.id = :idTipoTreino ORDER BY e.tipoTreino.nome ASC,e.posicao ASC ")
	public List<ExerciciosDaFicha> bucarExercicosDaFichaAndamento(@Param("idFicha") Long idFicha, @Param("idTipoTreino") Long idTipoTreino);
}
