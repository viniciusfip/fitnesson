package br.com.f2v.fitnesson.servicos;



import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Usuario;
import br.com.f2v.fitnesson.repositorios.UsuarioRepositorio;

@Service
public class UsuarioServico {
	
	@Inject
	private UsuarioRepositorio usuarioRepositorio;
	
	public Usuario salvarUsuario(Usuario usuario){
		return this.usuarioRepositorio.save(usuario);
	}
	
	public Usuario recuperaUsuario(String login,String senha){
		return this.usuarioRepositorio.listaUsuario(login, senha);
	}

}
