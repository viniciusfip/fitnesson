package br.com.f2v.fitnesson.entidades;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.f2v.fitnesson.enumeradores.EstadoCivilEnum;
import br.com.f2v.fitnesson.enumeradores.StatusFuncionarioEnum;

@Entity
@DynamicInsert
@DynamicUpdate
@Component
@Scope("prototype")
public class Funcionario extends Pessoa {
	
	private StatusFuncionarioEnum statusFuncionarioEnum;
	
	private String numMatricula;

	private String especialidade;
	
	private EstadoCivilEnum estadoCivil;
	
	private Date dataAdmissao;
	
	private Date dataDemissao;
	
	private Date dataCadastro;
	
	private Date aniversario;
	
	private String telefone1;
	
	private String telefone2;
	
	private String obs;
	
	@OneToMany(cascade = {CascadeType.ALL}, 
			mappedBy="funcionario", 
			fetch=FetchType.LAZY)
	private Set<FuncionarioEndereco> enderecos;
	
	@OneToOne
	private Cargo cargo;
	
	@Lob
	private byte[] foto;

	public StatusFuncionarioEnum getStatusFuncionarioEnum() {
		return statusFuncionarioEnum;
	}

	public void setStatusFuncionarioEnum(StatusFuncionarioEnum statusFuncionarioEnum) {
		this.statusFuncionarioEnum = statusFuncionarioEnum;
	}

	public String getNumMatricula() {
		return numMatricula;
	}

	public void setNumMatricula(String numMatricula) {
		this.numMatricula = numMatricula;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public EstadoCivilEnum getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivilEnum estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public Date getDataDemissao() {
		return dataDemissao;
	}

	public void setDataDemissao(Date dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getAniversario() {
		return aniversario;
	}

	public void setAniversario(Date aniversario) {
		this.aniversario = aniversario;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public Set<FuncionarioEndereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(Set<FuncionarioEndereco> enderecos) {
		this.enderecos = enderecos;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	
	

}
