package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;


import br.com.f2v.fitnesson.entidades.TipoTreino;
import br.com.f2v.fitnesson.repositorios.TipoTreinoRepositorio;

@Service
public class TipoTreinoServico {
	
	@Inject 
	private TipoTreinoRepositorio tipoTreinoRepositorio;
	
	public TipoTreino salvarTipoTreino(TipoTreino tipoTreino){
		return this.tipoTreinoRepositorio.save(tipoTreino);
	}
	
	public List<TipoTreino> listarTodos(){
		return this.tipoTreinoRepositorio.findAll();
	}
	
	public TipoTreino buscarPorId(long id){
		return this.tipoTreinoRepositorio.findOne(id);
	}
	
	public TipoTreino buscaPeloNome(String nome){
		return this.tipoTreinoRepositorio.buscaPeloNome(nome);
	}
	
	public List<TipoTreino> pesquisaPeloNome(String nome){
		return this.tipoTreinoRepositorio.pesquisaPeloNome(nome);
	}
	
	public void excluir(TipoTreino tipoTreino){
		this.tipoTreinoRepositorio.delete(tipoTreino);
	}
}
