package br.com.f2v.fitnesson.servicos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.f2v.fitnesson.entidades.Exercicio;
import br.com.f2v.fitnesson.repositorios.ExercicioRepositorio;

@Service
public class ExercicioServico {
	
	@Inject
	private ExercicioRepositorio exercicioRepositorio;
	
	public Exercicio salvarExercicio(Exercicio exercicio){
		return this.exercicioRepositorio.save(exercicio);
	}
	
	public List<Exercicio> listarTodosExercicios(){
		return this.exercicioRepositorio.findAll();
	}
	
	public Exercicio buscaPorId(long id){
		return this.exercicioRepositorio.findOne(id);
	}
	
	public List<Exercicio> buscarPorNome(String nome){
		return this.exercicioRepositorio.listaGrupoNome(nome);
	}
	
	public void excluirExercicio(Exercicio exercicio){
		this.exercicioRepositorio.delete(exercicio);
	}

}
