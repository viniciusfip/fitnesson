package br.com.f2v.fitnesson.enumeradores;

/**
 * 
 * Classe que representa as constantes de estado c�vil
 * @author Fabio Sicupira Cavalcante - sc.fabio@gmail.com
 *
 */
public enum EstadoCivilEnum {
	CASADO, DIVORCIADO, SOLTEIRO, VIUVO;
}
